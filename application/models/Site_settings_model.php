<?php
 
 /*
 * Project:	  Activ CMS Version 5
 * File:	  Site_settings_model.php 
 * Author:    Activ Developers
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 */
 
class Site_settings_model extends CI_Model 
{
	var $affected_rows = 0;

	public function __construct()
	{
		parent::__construct();

	}

	public function getSettings(){
      	//load basic site settings into array
      	$sql = "SELECT * FROM config";
		$query = $this->db->query($sql);

 		if($query->num_rows() > 0 ){
		    foreach($query->result('array') as $record){
				$aConfig[$record['key_option']] = $record['key_value'];
		    } 

			return $aConfig;
      	}			
	}

	public function updateSiteSetup($data, $main = '', $global = ''){
		
		unset($data['update']);

		if($main){
			if (isset($data['site_online'])){
				$data['site_online'] = 1;
			}else{
				$data['site_online'] = 0;			
			}			
		}

		if($global){
			if (isset($data['show_cookie_on_header'])){
				$data['show_cookie_on_header'] = 1;
			}else{
				$data['show_cookie_on_header'] = 0;			
			}	

			if (isset($data['show_sitemap_on_footer'])){
				$data['show_sitemap_on_footer'] = 1;
			}else{
				$data['show_sitemap_on_footer'] = 0;			
			}			
		}

		if(isset($data['default_font_size'])){
			$data['default_font_size'] = preg_replace('/[^0-9]/', '', $data['default_font_size']);
		}

		if(isset($data['website_px_width'])){
			$data['website_px_width'] = preg_replace('/[^0-9]/', '', $data['website_px_width']);
		}

		if(isset($data['website_percent_width'])){
			$data['website_percent_width'] = preg_replace('/[^0-9]/', '', $data['website_percent_width']);
		}



		foreach ($data as $key => $value){
			$sql = sprintf('UPDATE config SET key_value = "%s" WHERE key_option = "%s"', mysql_escape_string($value), $key);		
			$query = $this->db->query($sql); 	
			$this->affected_rows = $this->affected_rows+$this->db->affected_rows();
		}
		
      	if($this->affected_rows > 0){
        	return true;
      	}		
	}

	public function getWebFonts(){
		
      	$sql = "SELECT * FROM web_fonts";
		$query = $this->db->query($sql);	

 		if($query->num_rows() > 0 ){
		    foreach($query->result('array') as $record){
				$aWebFonts[] = $record;
		    } 
			return $aWebFonts;
      	}		
	}
	

}
?>
