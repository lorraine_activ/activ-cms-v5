<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Password_reset_model extends CI_Model 
{
    public $user_id, $token, $request_ts;

    /**
     * Constructor.
     * @param null|array $data
     */
    public function __construct($data = null)
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->helper('string');

        if ($data !== null && is_array($data)) {
            $this->user_id = $data['user_id'];
            $this->token = $data['token'];
            $this->request_ts = $data['request_ts'];
        }
    }

    /**
     * Creates a PasswordReset token in the database
     *
     * @param int $user_id
     * @return bool|Passwordreset
     */
    public function create($user_id = null)
    {
        if ($user_id == null || $user_id === false) {
            return false;
        }
        $data = array(
            'user_id' => $user_id,
            'token' => random_string(),
            'request_ts' => time(),
        );

        return $this->db->insert('password_resets', $data) ? (object) ($data) : false;
    }

    /**
     *
     * Either returns password reset information given an ID and a token, or returns false.
     *
     * @param $user_id
     * @param $token
     *
     * @return bool|Passwordreset
     */
    public function get($user_id = null, $token = null)
    {

        if ($user_id == null || $token == null) {
            return false;
        }
        $query = $this->db->select('*')->from('password_resets')->where('user_id', $user_id)->where('token',
            $token)->get();
        return ($query && $query->num_rows() > 0) ? $query->row() : false;
    }

    /**
     * Returns password reset information given a token
     *
     * @param null $token
     * @return bool|Passwordreset
     */
    public function get_from_token($token = null)
    {
        if ($token == null) {
            return false;
        }
        $query = $this->db->select('*')->from('password_resets')->where('token', $token)->get();
        return ($query && $query->num_rows() > 0) ? $query->row() : false;
    }

    /**
     *
     * Either returns password reset information given an email and a token, or returns false.
     *
     * @param $user_email
     * @param $token
     *
     * @return bool|Passwordreset
     */
    public function get_with_email($user_email, $token)
    {
        if ($user_email == null || $token == null) {
            return false;
        }
        $query = $this->db->select('*')->from('password_resets')->where("`user_id` IN (SELECT `USER_ID` FROM `Users` WHERE `USER_Name`='$user_email'",
            null, false)->where('token', $token)->get();
        return ($query && $query->num_rows() > 0) ? $query->row() : false;
    }

    /**
     * Removes all password reset information for a particular user ID
     *
     * @param $user_id
     * @return bool Whether or not a call was made to delete password resets for the particular user ID
     */
    public function clear($user_id)
    {
        if ($user_id == null) {
            return false;
        }
        $this->db->delete('password_resets', array(
            'user_id' => $user_id
        ));
        return true;
    }

    /**
     * Checks if the request has expired or not
     *
     * @todo Replace (60 * 60 * 24) with a configurable value
     * @param $passwordreset
     * @return bool True if the request time is in the past and not too far in the future (currently, 24 hours)
     */
    public function has_expired($passwordreset)
    {

        $current_time = time();

        return !$this->in_range($passwordreset->request_ts, $current_time, $passwordreset->request_ts + (60 * 60 * 24));

    }

    public function in_range($min, $to_check, $max)
    {
        return $min <= $to_check && $to_check <= $max;
    }

    public function generateResetPasswordLink($token)
    {
        return base_url() . 'reset_password_form?token=' . $token;
    }

    public function generate_email_forgot_password($url_reset_password, $token, $hours_until_expiration)
    {
        $pluralized_hours = intval($hours_until_expiration) === 1 ? 'hour' : intval($hours_until_expiration) . ' hours';

        $data = [
            'url' => $url_reset_password.'?token='.$token,
            'hours' => $pluralized_hours

        ];
        return $data;
    }

    public function generate_email_reset_password($url_reset_password, $token, $hours_until_expiration)
    {
        $pluralized_hours = intval($hours_until_expiration) === 1 ? 'hour' : intval($hours_until_expiration) . ' hours';
        $data = [
            'url' => $url_reset_password.'?token='.$token,
            'hours' => $pluralized_hours
        ];
        return $data;
    }

}