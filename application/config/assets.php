<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');



 /*
 * Project:	  Activ CMS Version 5
 * File:	  config/assets.php
 * Author:    Activ Developers
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 */

/*
| -------------------------------------------------------------------
| ASSETS HELPER
| -------------------------------------------------------------------
| This file contains four arrays of user agent data. It is used by the
| User Agent Class to help identify browser, platform, robot, and
| mobile device data. The array keys are used to identify the device
| and the array values are used to set the actual name of the item.
*/

// Base path. URLs will be /path_base/path_<asset type>/<asset_name>
$config['path_base'] = 'assets';

// Read above. Path for asset types.
$config['path_js'] = 'js';
$config['path_css'] = 'css';
$config['path_img'] = 'img';
