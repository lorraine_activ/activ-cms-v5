<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 /*
 * Project:	  Activ CMS Version 5
 * File:	  config/form_validation.php 
 * Author:    Activ Developers
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 * form validation rules
 */
$config = array(
	
	'update_users' => array(         	
		array(
			'field' => 'firstname',
			'label' => 'First Name',
			'rules' => 'strip_tags|required|max_length[100]|trim'
		),
		array(
			'field' => 'lastname',
			'label' => 'Last Name',
			'rules' => 'strip_tags|required|max_length[100]|trim'
		),
		array(
			'field' => 'email',
			'label' => 'Email Address',
			'rules' => 'strip_tags|required|valid_email|max_length[100]|trim|callback_validate_email'
		),
		array(
			'field' => 'role',
			'label' => 'Select User Role',
			'rules' => 'required'
		)
	),

	'confirm_passwords' => array(                 	
		array(
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'strip_tags|required|min_length[6]|trim'
		),
		array(
			'field' => 'confirm_password',
			'label' => 'Confirm Password',
			'rules' => 'strip_tags|required|matches[password]|min_length[6]|trim'
		)
	),

	'add_users' => array(  
		array(
			'field' => 'role',
			'label' => 'Select User Role',
			'rules' => 'required'
		),       	
		array(
			'field' => 'firstname',
			'label' => 'First Name',
			'rules' => 'strip_tags|required|max_length[100]|trim'
		),
		array(
			'field' => 'lastname',
			'label' => 'Last Name',
			'rules' => 'strip_tags|required|max_length[100]|trim'
		),
		array(
			'field' => 'email',
			'label' => 'Email Address',
			'rules' => 'strip_tags|required|valid_email|max_length[100]|trim|callback_validate_email'
		),
		array(
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'strip_tags|required|min_length[6]|trim'
		),
		array(
			'field' => 'confirm_password',
			'label' => 'Confirm Password',
			'rules' => 'strip_tags|required|matches[password]|min_length[6]|trim'
		)
	),

	'update_pages' => array(         	
		array(
			'field' => 'name',
			'label' => 'Page Name',
			'rules' => 'strip_tags|required|max_length[100]|trim|callback_validate_slug'
		)
	),

	'update_custom_page' => array(         	
		array(
			'field' => 'name',
			'label' => 'Page Name',
			'rules' => 'strip_tags|required|max_length[100]|trim'
		)
	),

	'add_pages' => array(         	
		array(
			'field' => 'name',
			'label' => 'Page Name',
			'rules' => 'strip_tags|required|max_length[100]|trim|callback_validate_slug'
		)
	),

	'add_custom_page' => array(         	
		array(
			'field' => 'name',
			'label' => 'Page Name',
			'rules' => 'strip_tags|required|max_length[100]|trim'
		)
	)                           
);//end
