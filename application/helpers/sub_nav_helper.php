<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /*
 * Project:	  Activ CMS Version 5
 * File:	  helper/site_settings_nav.php
 * Author:    Marc Lorraine Jose
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 * helpers for the site settings module
 */


if ( ! function_exists('render_subnav'))
{
    function render_subnav($active = '')
    {
		$html = '';
		
		switch ($active) {
			
			/***Admin > Site Settings Nav***/
			case 'site_settings':		
				$html .= '<ul class="system_subnavs" id="site_settings_submenu">';	
				$html .= '<li class="subon">Activ Site Settings</li>';
				$html .= '<li><a href="'.base_url() . 'admin/google_settings">Google Settings</a></li>';
				$html .= '</ul>';
				break;
			case 'google_settings':
				$html .= '<ul class="system_subnavs" id="google_settings_submenu">';					
				$html .= '<li><a href="'.base_url() . 'admin/site_settings">Activ Site Settings</a></li>';
				$html .= '<li class="subon">Google Settings</li>';
				$html .= '</ul>';
				break;

			/***Admin > Users Nav***/
			case 'users';
				$html .= '<ul class="system_subnavs" id="users_submenu">';
				$html .= '<li class="subon">Activ Users</li>';
				$html .= '<li><a href="'.base_url() . 'admin/admin_users">Admin Access & Plugins</a></li>';
				$html .= '</ul>';
				break;

			/***Admin > Global Settings Nav***/
			case 'global_settings';
				$html .= '<ul class="system_subnavs" id="global_submenu">';
				$html .= '<li class="subon">Global Settings</li>';
				$html .= '<li><a href="'.base_url() . 'admin/global_css">Global CSS Styles</a></li>';
				$html .= '<li><a href="'.base_url() . 'admin/global_js">Global JS Scripts</a></li>';
				$html .= '</ul>';
				break;
			case 'global_css';
				$html .= '<ul class="system_subnavs" id="global_submenu">';
				$html .= '<li><a href="'.base_url() . 'admin/global_settings">Global Settings</a></li>';
				$html .= '<li class="subon">Global CSS Styles</li>';
				$html .= '<li><a href="'.base_url() . 'admin/global_js">Global JS Scripts</a></li>';
				$html .= '</ul>';
				break;
			case 'global_js';
				$html .= '<ul class="system_subnavs" id="global_submenu">';
				$html .= '<li><a href="'.base_url() . 'admin/global_settings">Global Settings</a></li>';
				$html .= '<li><a href="'.base_url() . 'admin/global_css">Global CSS Styles</a></li>';
				$html .= '<li class="subon">Global JS Scripts</li>';
				$html .= '</ul>';
				break;
		}

		return $html;
    } 
}
