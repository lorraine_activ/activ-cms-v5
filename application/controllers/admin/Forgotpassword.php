<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 /*
 * Project:	  Activ CMS Version 5
 * File:	  admin/ForgotPassword.php 
 * Author:    Activ Developers
 * Date		  February 2018
 * @copyright	2017 Activ Web Design 
 */

class Forgotpassword extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct(); 
		$this->load->model(array('user_model','password_reset_model'));
		$this->load->library('email');

	}

	public function index()
	{
		$this->load->view('admin/passwords');

	}

	public function reset_password($id)
	{

		$data['id'] = $id;
		$this->load->view('admin/reset_passwords',$data);
	}

	public function process_password_recovery()
    {
        $return = array('success' => false);
        try {
            $this->form_validation->set_rules(array(
                array('field' => 'email', 'label' => 'email address', 'rules' => 'trim|required|valid_email'),
            ));
            $email = $this->input->post('email');
            if ($this->form_validation->run() === false) {
                $return['message'] = validation_errors();
            } else {
                $return['success'] = true;
                if(($user_id = $this->user_model->checkActivEmail($email)) !== null) {
                    $reset_token = $this->password_reset_model->create($user_id);
                    //show error if email not exist
             		if(!$reset_token) redirect('','refresh');
                   	else $this->email_forgot_password($reset_token, $email);

	                $return['message'] = "Reset password link has been sent. Please check your email.";
	                $this->session->set_flashdata('msg', $return['message']);
                   	redirect('admin/login','refresh');
                } else {
                	$return['message'] = 'Reset Password Error!';
	                $this->session->set_flashdata('msg', $return['message']);
                	redirect('','refresh');
                }
            }
        }
        catch(Exception $e) {
            $return['message'] = "System Error. Please try again later.";
            $return['success'] = false;
        }
        header('Content-Type: application/json');
        echo json_encode($return);
    }

    public function activate_new_password()
    {
    	$this->load->library('form_validation');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|matches[password]');
		if($this->form_validation->run() == FALSE){
			$this->form_validation->set_message('Warning', 'Password Validation Error');
			$return['message'] = 'Password Validation Error';
			$this->session->set_flashdata('msg', $return['message']);
			redirect('reset_password/'.$this->input->post('user_id'),'refresh');
		}else{
			$post = $this->input->post();
			$user = $this->user_model->getUserById($post['user_id']);
			$return = $this->user_model->update_user_password($post);
			if($return)
			{
	            $html = $this->generate_email_reset_password_successfully();
	            $CI =& get_instance();
	            $CI->config->load('activ_emails', true);
	            $config = $CI->config->item('activ_emails');
	            $this->email->initialize($config);
	            $this->email->from($config['smtp_user'], 'Joel Espadilla');
	            $this->email->to($user['email']);
	            $this->email->subject('Password Reset Successful');
	            $this->email->message($html);
	            if($this->email->send()){
	            	$return['message'] = "Password has been changed.";
	                $this->session->set_flashdata('msg', $return['message']);
	                redirect('admin','refresh');
	            }else{
	                //add code in case error;
	                $return['message'] = "System error in sending email. Password has been changed.";
	                $this->session->set_flashdata('msg', $return['message']);
	                redirect('admin','refresh');
	            }
			}

			// apply error handling
		}

    }
    
    private function email_forgot_password($passwordreset, $email)
    {
        try {
            $reset_url = base_url() . 'reset_password_form';
            // Generate the password email
            $body = $this->password_reset_model->generate_email_forgot_password($reset_url, $passwordreset->token, 24);
            $html = <<<RETURN
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Forgot Password</title>
    </head>
    <body>
        <p>We received a request to reset your password . To reset your password, please
        use the following link:</p>
        <p><a href="{$body['url']}&id={$passwordreset->user_id}">{$body['url']}</a></p>
        <p>As a security precaution, this link will expire in {$body['hours']}.</p>
        <p>If you had not made this request, please contact your account manager.</p>
    </body>
</html>
RETURN;

			$this->load->library('email');
			$CI =& get_instance();
            $CI->config->load('activ_emails', true);
            $config = $CI->config->item('activ_emails');

			$this->email->initialize($config);

			$this->email->from($config['smtp_user'], 'Joel Espadilla');
			$this->email->to($email);
			$this->email->subject('Forgot Password');
			$this->email->message($html);


            if($this->email->send()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            error_log($e->getMessage());
            return false;
        }
    }

    private function generate_email_reset_password_successfully()
    {
        $base_url = base_url();
        return <<<RETURN
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title> Password Reset Successfully</title>
    </head>
    <body>
        <p>Your may now use your new password:</p>
        <p><a href="{$base_url}admin">{$base_url}admin</a></p>
    </body>
</html>
RETURN;
    }

}
