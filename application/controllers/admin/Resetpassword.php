<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Resetpassword Controller
 *
 */
class Resetpassword extends CI_Controller
{
    /**
     * Constructor
     * @helper pass
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array( 'form', 'url', 'string'));
        $this->load->model(array('password_reset_model', 'user_model'));
    }

    public function processResetPassword()
    {
        $return = array('success' => false);

        try {
            $token = $this->input->get('token');
            $id = $this->input->get('id');
            $reset = $this->password_reset_model->get_from_token($token);
            
            if ($reset === false) {
                $return['message'] = 'This token is either invalid or has expired.';
            }  else if ($this->password_reset_model->has_expired($reset)) {
                $return['message'] = 'This password reset request has expired.';
            } else {
                $user = $this->user_model->getUserById($id) ;
                redirect('reset_password/'.$user['id'],'refresh');
                
            }
        } catch (Exception $e) {
            $return['message'] = "Unfortunately, the system had a problem processing your request. Please try again later.";
            $return['success'] = false;
            $this->session->set_flashdata('msg', $return['message']);
        }
        header('Content-Type: application/json');
        echo json_encode($return);
    }

    
}