<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 /*
 * Project:	  Activ CMS Version 5
 * File:	  Ajax_admin.php 
 * Author:    Activ Developers
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 */
 
class Ajax_admin extends CI_Controller 
{
	
	public function __construct()
	{
		parent::__construct(); 
		$this->load->model(array('pages_model'));
	}

	function index()
	{ 

		//TODO: check http request before performing tasks

		//check file access  
    	if(!$this->auth_model->checkAdmin()){
    		redirect('admin/login_view','refresh');
    	}

        switch($_POST['action'])
        {
			//get user details
          	case 'get-user':  
				$aUser = $this->auth_model->getUserById($_POST['user_id']);
            	if($aUser){
					header('Content-Type: application/json');
					echo json_encode($aUser);
            	}
          	break;

			//add user details
          	case 'add-user':
          		
          		$aUser = $this->auth_model->addUser($_POST);
				if($aUser){
					header('Content-Type: application/json');
					echo json_encode($aUser);			
				}
          	break;

			//update user details
          	case 'update-user':
          		
          		$aUser = $this->auth_model->updateUser($_POST);
				if($aUser){
					header('Content-Type: application/json');
					echo json_encode($aUser);			
				}
          	break;

			//delete user
          	case 'delete-user':

				if($this->auth_model->deleteUser($_POST['user_id'])){
					header('Content-Type: application/json');
					echo true;		
				}
          	break;

			//delete user
          	case 'sort-pages':

				if($this->pages_model->sortPages($_POST['seq'])){
					header('Content-Type: application/json');
					echo true;		
				}
          	break;


		}

	}

	function login()
	{
        switch($this->input->post('action'))
        {
       		//Process Login
          	case 'process-login':  
            	if($this->auth_model->process_admin_login($this->input->post('lg_username'), $this->input->post('lg_password'))){
              		// header('Content-Type: text/xml; charset=utf-8');
              		// echo '<success valid="yes">1</success>';
              		redirect( 'admin/dashboard','refresh');
            	}else{	
              		header('Content-Type: text/xml; charset=utf-8');
              	echo '<success valid="no">1</success>';
            	}
          	break;
		}	
	}
}
?>
