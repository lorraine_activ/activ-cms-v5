<?php defined('BASEPATH') OR exit('No direct script access allowed');

 /*
 * Project:	  Activ CMS Version 5
 * File:	  admin/Site_settings.php
 * Author:    Activ Developers
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 */
 
class Global_settings extends CI_Controller
{

	public function __construct()
	{
		parent::__construct(); 
		$this->load->model(array('site_settings_model'));
		$this->load->helper('sub_nav');

		//check admin already logged in and has access to site settings, if not, redirect to dashboard
    	if(!$this->auth_model->checkAdmin('global_settings')){
    		redirect('admin/login_view','refresh');
    	}	
	}
	
	function index()
	{ 

		//Update activ main settings
		if($this->input->post('update') && $this->input->post('update') == 'Update'){
		    if($this->site_settings_model->updateSiteSetup($_POST, '', 1)){	    	
		      	$data['sSuccess'] = "Global settings has been updated successfully.";
		    }
		}
		
		//Get site settings
    	$data['aSiteConfig'] = $this->site_settings_model->getSettings();

		//get web fonts
    	$data['aWebFonts'] = $this->site_settings_model->getWebFonts();

		//Render Sub Nav
		$data['aSubnav'] = render_subnav('global_settings');

		//Page Name
		$data['aPageName'] = 'global_settings';

		//JS File
		$data['aJsFiles'] = 'admin/global_settings';

		$data['isColorPicker'] = 1;
		$data['isFancyBox'] = '1';

		//Load variables
		$this->load->vars($data);   

		//render template
		$this->load->view('admin/_header_view.php');
		$this->load->view('admin/global_settings_view.php');
		$this->load->view('admin/_footer_view.php');
  	}


	function global_css()
	{ 
		if($this->input->post('update') && $this->input->post('update') == 'Update'){
			
			if($_POST['advance_css']){
				$fh = fopen(FCPATH . "assets/css/global_css.css", w);
				fwrite($fh, stripslashes($_POST['advance_css']));
				fclose($fh);
				$data['sSuccess'] = "The css codes has been updated successfully.";
			}
			
		}

		//get CSS content
		$data['aGlobalCss'] = file_get_contents(FCPATH . "assets/css/global_css.css");

		//Render Sub Nav
		$data['aSubnav'] = render_subnav('global_css');

		//Page Name
		$data['aPageName'] = 'global_css';

		//JS File
		$data['aJsFiles'] = 'admin/global_css';
	
		$data['isEditArea'] = 1;

		//Load variables
		$this->load->vars($data);   

		//render template
		$this->load->view('admin/_header_view.php');
		$this->load->view('admin/global_css_view.php');
		$this->load->view('admin/_footer_view.php');
  	}

	function global_js()
	{ 
		if($this->input->post('update') && $this->input->post('update') == 'Update'){
			
			if($_POST['advance_js']){
				$fh = fopen(FCPATH . "assets/js/global_js.js", w);
				fwrite($fh, stripslashes($_POST['advance_js']));
				fclose($fh);
				$data['sSuccess'] = "The javascript codes has been updated successfully.";
			}
			
		}

		//get CSS content
		$data['aGlobalJs'] = file_get_contents(FCPATH . "assets/js/global_js.js");

		//Render Sub Nav
		$data['aSubnav'] = render_subnav('global_js');

		//Page Name
		$data['aPageName'] = 'global_js';

		//JS File
		$data['aJsFiles'] = 'admin/global_js';
	
		$data['isEditArea'] = 1;

		//Load variables
		$this->load->vars($data);   

		//render template
		$this->load->view('admin/_header_view.php');
		$this->load->view('admin/global_js_view.php');
		$this->load->view('admin/_footer_view.php');
  	}

}
