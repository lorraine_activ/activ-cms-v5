<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 /*
 * Project:	  Activ CMS Version 5
 * File:	  admin/File_and_image.php 
 * Author:    Activ Developers
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 */

class File_and_image extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct(); 
	}

	public function index()
	{
		
		//Page Name
		$data['aPageName'] = 'file_and_image';
		//Load variables
		$this->load->vars($data);   

		//render template
		$this->load->view('admin/_header_view.php');
		$this->load->view('admin/file_and_image_view.php');
		$this->load->view('admin/_footer_view.php');
	}



}
