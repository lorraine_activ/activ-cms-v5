<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 /*
 * Project:	  Activ CMS Version 5
 * File:	  admin/Site_settings.php
 * Author:    Activ Developers
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 */
 
class Users extends CI_Controller
{

	public function __construct()
	{
		parent::__construct(); 
		$this->load->model(array('site_settings_model'));
		$this->load->helper('sub_nav');
		$this->load->library('form_validation');

		//check admin already logged in and has access to site settings
    	if(!$this->auth_model->checkAdmin('users')){
    		$aDashboard = $this->config->item('site_url').'admin/';
			header("Location: " . $aDashboard);
			die();
    	}
	}
	
	function index()
	{ 

    	//delete user
   		if($this->input->post('action') && $this->input->post('action') == 'delete'){
			if($this->auth_model->deleteUser($this->input->post('id'))){
		    	$data['sSuccess'] = "User record was removed successfully";     
		    	$_POST['id'] = '';
		  	}
	  	}

		//add users
		if($this->input->post('add') && $this->input->post('add') == 'Add'){

			if ($this->form_validation->run('add_users') == FALSE){
				$data['sError'] = validation_errors();
			}else{
				$_POST['id'] = $this->auth_model->addUser($_POST);
				if($this->auth_model->success){
					$data['sSuccess'] = $this->auth_model->success;
				}else{
					$data['sError'] = $this->auth_model->error;	
				}
			}
		}

		//update users
		if($this->input->post('update') && $this->input->post('update') == 'Update'){
			//confirm password
			if($this->input->post('password') || $this->input->post('confirm_password')){
				if ($this->form_validation->run('confirm_passwords') == FALSE){
					$data['sError'] = validation_errors();
				}
			}

			if ($this->form_validation->run('update_users') == FALSE){
				$data['sError'] = validation_errors();
			}else{

				$this->auth_model->updateUser($_POST);
				if($this->auth_model->success){
				  $data['sSuccess'] = $this->auth_model->success;
				}
			}
		}

		if($_POST['id']){
			$data['sId'] = $_POST['id'];
			//get user by id
			$data['aUser'] = $this->auth_model->getUserById($_POST['id']);
		}

		//show all users based on role: 1=admin, 2=franchisee, 3=client
		$data['aUsers'] = $this->auth_model->getUsers($this->session->userdata('admin')['role']);

		//Render Sub Nav
		$data['aSubnav'] = render_subnav('users');

		//Page Name / JS file
		$data['aPageName'] = $data['aJsFiles'] = 'users';

		//Load variables
		$this->load->vars($data);   

		//render template
		$this->load->view('admin/_header_view.php');
		$this->load->view('admin/users_view.php');
		$this->load->view('admin/_footer_view.php');
  	}


	function validate_email($email){

    	$result = $this->auth_model->checkUniqueEmail($email);

    	if($result['fail'] == 1){
			$this->form_validation->set_message('validate_email', 'Sorry, it appears a user <strong>('.$result['full_name'] . ')</strong> is already registered with email <strong>'. $email.'</strong>');
			return FALSE;
		}else{
			return TRUE;
		}	
	}

}
