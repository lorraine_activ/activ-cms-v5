<?php defined('BASEPATH') OR exit('No direct script access allowed');

 /*
 * Project:	  Activ CMS Version 5
 * File:	  admin/Page.php
 * Author:    Marc Lorraine Jose
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 */
 
class Pages extends CI_Controller
{

	public function __construct()
	{
		parent::__construct(); 
		$this->load->model(array('pages_model'));
		$this->load->helper('sub_nav');

		//check admin already logged in and has access to site settings, if not, redirect to dashboard
    	if(!$this->auth_model->checkAdmin('pages')){
    		redirect('','refresh');
    	}	

	}
	
	public function index($id)
	{ 

		if($id){
			$data['sId'] = $id;
			//get user by id
			$data['aPage'] = $this->pages_model->getPageById($_POST['id']);
		}

		
		$data['isSortable'] = '1';

		//Get pages
    	$data['aPages'] = $this->pages_model->getPages();

		//Render Sub Nav
		$data['aSubnav'] = render_subnav('pages');

		//Page Name
		$data['aPageName'] = 'pages';

		$data['aJsFiles'] = 'admin/pages';
		//Load variables
		$this->load->vars($data);   

		//render template
		$this->load->view('admin/_header_view');
		$this->load->view('admin/pages_view');
		$this->load->view('admin/_footer_view');
  	}

	//validate slugs
	public function validate_slug($data){

    	$result = $this->pages_model->checkUniqueSlug($data['name'], $data['id']);

    	if($result == false){
			$this->form_validation->set_message('validate_slug', $this->pages_model->error);
			return FALSE;
		}else{
			return TRUE;
		}	
	}

	/**
	 * Create or Add Pages
	 * 
 	 */

	public function add_user_page()
	{
		$post = $this->input->post();
		//TODO: revalidate page name
		if($post['type'] == "2"){
			$validation = 'add_custom_page';
		}else{
			$validation = 'add_pages';				
		}

		if ($this->form_validation->run($validation) == FALSE){
			$data['sError'] = validation_errors();
		}else{
			
			$data['id'] = $this->pages_model->addPage($post);
			if($this->pages_model->success){
				$data['sSuccess'] = $this->pages_model->success;
			}else{
				$data['sError'] = $this->pages_model->error;	
			}
			redirect('admin/pages','refresh');
		}
	}

	/**
	 * Update or Edit Pages
	 *
	 */

	public function update_user_page()
	{
		//update pages
		$post = $this->input->post();
		
		
		if($post['type'] == "2"){
			$validation = 'update_custom_page';
		}else{
			$validation = 'update_pages';				
		}
		
		if ($this->form_validation->run($validation) == FALSE){
			$data['sError'] = validation_errors();
		}else{
			$this->pages_model->updatePage($post);
			if($this->pages_model->success){
			  $data['sSuccess'] = $this->pages_model->success;
			}
		}
		redirect('admin/pages','refresh');

	}

	public function delete_user_page()
	{
		if($this->pages_model->deletePage($this->input->post('id'))){
	    	$data['sSuccess'] = "The page was removed successfully";     
	  	}
	}

}
