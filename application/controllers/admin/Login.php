<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 /*
 * Project:	  Activ CMS Version 5
 * File:	  admin/Dashboard.php 
 * Author:    Activ Developers
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 */

class Login extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct(); 
	}

	public function index()
	{
		//check admin already logged in and has access to dashboard
    	if($this->auth_model->checkAdmin('dashboard')){
   			// $aDashboard = $this->config->item('site_url').'admin/dashboard';
			// header("Location: " . $aDashboard);
			$this->dashboard();
    	}else{
    		
			//Page Name
			$data['aPageName'] = 'login';
			//Load JS Files
			$data['aJsFiles'] = 'login';
			//Load variables
			$this->load->vars($data);   

			//render template
			$this->load->view('admin/login_view');
    	}	

	}


	public function dashboard()
	{

		//Page Name
		$data['aPageName'] = 'dashboard';
		//Load variables
		$this->load->vars($data);   

		//render template
		$this->load->view('admin/_header_view');
		$this->load->view('admin/dashboard_view');
		$this->load->view('admin/_footer_view');
	}

	public function logout()
	{
		$this->auth_model->logout('admin');
		redirect('','refresh');

	}

	public function forgot_password()
	{
		$this->load->view('admin/passwords');
		
	}

}
