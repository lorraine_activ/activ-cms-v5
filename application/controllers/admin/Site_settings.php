<?php defined('BASEPATH') OR exit('No direct script access allowed');

 /*
 * Project:	  Activ CMS Version 5
 * File:	  admin/Site_settings.php
 * Author:    Activ Developers
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 */
 
class Site_settings extends CI_Controller
{

	public function __construct()
	{
		parent::__construct(); 
		$this->load->model(array('site_settings_model'));
		$this->load->helper('sub_nav');

		if(!$this->auth_model->checkAdmin('site_settings')){
    		redirect('','refresh');
    	}

	}
	
	public function index()
	{ 

		
		
		//Get site settings
    	$data['aSiteConfig'] = $this->site_settings_model->getSettings();

		//Render Sub Nav
		$data['aSubnav'] = render_subnav('site_settings');

		//Page Name
		$data['aPageName'] = 'site_settings';

		$data['aJsFiles'] = 'admin/site_settings';
		$data['isFancyBox'] = 1;
		//Load variables
		$this->load->vars($data);   

		//render template
		$this->load->view('admin/_header_view.php');
		$this->load->view('admin/site_settings_view.php');
		$this->load->view('admin/_footer_view.php');
  	}


	public function google_settings()
	{ 

		
		
		//Get site settings
    	$data['aSiteConfig'] = $this->site_settings_model->getSettings();

		//Render Sub Nav
		$data['aSubnav'] = render_subnav('google_settings');

		//Page Name
		$data['aPageName'] = 'google_settings';

		//Load variables
		$this->load->vars($data);   

		//render template
		$this->load->view('admin/_header_view.php');
		$this->load->view('admin/google_settings_view.php');
		$this->load->view('admin/_footer_view.php');
  	}

	public function responsive_settings()
	{ 

		//Render Sub Nav
		$data['aSubnav'] = render_subnav('responsive_settings');

		//Page Name
		$data['aPageName'] = 'responsive_settings';

		//Load variables
		$this->load->vars($data);   

		//render template
		$this->load->view('admin/_header_view.php');
		$this->load->view('admin/responsive_settings_view.php');
		$this->load->view('admin/_footer_view.php');
  	}

	public function reset_website()
	{ 

		//Render Sub Nav
		$data['aSubnav'] = render_subnav('reset_website');

		//Page Name
		$data['aPageName'] = 'reset_website';

		//Load variables
		$this->load->vars($data);   

		//render template
		$this->load->view('admin/_header_view.php');
		$this->load->view('admin/reset_website_view.php');
		$this->load->view('admin/_footer_view.php');
  	}

  	/*
  	 * Update active site settings
  	 */
  	public function update_active_settings()
  	{
  		$post=$this->input->post();
  		 //Update activ main settings
		if($this->input->post('update') && $this->input->post('update') == 'Update'){
		    if($this->site_settings_model->updateSiteSetup($_POST, 1)){	    	
		      	$data['sSuccess'] = "Site configuration has been updated successfully.";
		    }
		}
		redirect('admin/site_settings','refresh');

  	}

  	public function update_google_settings()
  	{
  		$post=$this->input->post();
  		//Update google settings
		if($this->input->post('update') && $this->input->post('update') == 'Update'){
		    if($this->site_settings_model->updateSiteSetup($_POST)){	    	
		      	$data['sSuccess'] = "Google settings has been updated successfully.";
		    }
		}

		redirect('admin/google_settings','refresh');
  	}
}
