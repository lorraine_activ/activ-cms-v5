<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct(); 


	}

	function index()
	{
		
		//check if site is online
        if (!$this->config->item('site_online')) {
            $this->load->view('_offline_view.php');
            return false;
        }



		$this->load->view('_header_view');
		$this->load->view('page_view');
		$this->load->view('_footer_view');
	}
}
