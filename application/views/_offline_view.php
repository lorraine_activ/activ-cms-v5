<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=$this->config->item('site_language')?>">
<head>
<title><?=$this->config->item('site_name')?></title>
<meta http-equiv="Content-Type" content="text/html; <?=$this->config->item('charset')?>" />
<meta http-equiv="content-language" content="<?=$this->config->item('site_language')?>" />     
<meta name="robots" content="all" />
<meta name="author" content="<?=$this->config->item('author_name')?> - <?=$this->config->item('author_url')?>" /> 
<base href="<?=base_url()?>" />
<style type="text/css">
* { text-align: center; font-family: Georgia; }
h1 { font-size: 36px; }
h1 a { color: #f78F1e; text-decoration: none; }
img { border: 0; }
</style>
</head>
<body>
	<h1><a href="./"><?=$this->config->item('site_name')?></a></h1>
	<p>Sorry this site is currently offline, please check back again soon.</p>
	<p><a href="http://www.activwebdesign.com" target="_blank"><?=assets_img('activ_logo.png', array('alt' => 'Activ Web Design', 'width' => 329))?></a></p>
</body>
</html>

