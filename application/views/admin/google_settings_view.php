<?php

 /*
 * Project:	  Activ CMS Version 5
 * File:	  google_settings_view.php
 * Author:    Activ Developers
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 */

?>

<div class="activ_box col-md-9">

	<p class="system_h1">Activ Google Settings</p>
	<p class="intro_desc">In this section you can enter the tracking code for Google Analytics, the META tag verifier for Google Webmaster Tools and will provide you location of the website's sitemaps.</p>


	<div class="google_settings_form">

		<?if ($sSuccess){?>
		<div id="formSuccess">
		  <?=$sSuccess?>
		</div>
		<?}?>

		<?if ($sError){?>
		<div id="formError">
		  <?=$sError?>
		</div>
		<?}?>

		<?=form_open('admin/site_settings/update_google_settings')?>

			<div class="row">
				<div class="form-group col-md-8">
					<label for="settings_sei">Search Engine Indexing</label>
					<div class="radio">
						<input type="radio" name="sei" id="radio1" <?=($aSiteConfig['sei'] == 1 ? 'checked=checked' : '')?> value="1">
						<label for="radio1">
						    Site is live. Allow site to be indexed
						</label>
					</div>
					<div class="radio">
						<input type="radio" name="sei" id="radio2" <?=($aSiteConfig['sei'] == 0 ? 'checked=checked' : '')?> value="0">
						<label for="radio2">
						    Site is in development. Prevent from being indexed
						</label>
					</div>
				</div>

				<div class="col-md-1">
					<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
						title="When a site is in development we want to prevent it from being indexed by search engines such as Google, Bing, Yahoo etc. We only want search engines to index our site when it is live and has its own domain.">
						<?=assets_img('icon_help.gif')?>
					</a>
				</div>
			</div>

			<div class="row">
		    	<div class="form-group col-md-8">
					<label for="">Google Analytics Tracking Code</label>
					<textarea name="google_analytics" class="form-control" id="" rows="10"><?=$aSiteConfig['google_analytics']?></textarea>
		  		</div>

				<div class="col-md-1">
					<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
						title="Here you can paste in the JavaScript tracking code provided by Google Analytics. You can read more about Google Analytics and sign up at http://www.google.com/analytics">
						<?=assets_img('icon_help.gif')?>
					</a>
				</div>
			</div>

			<div class="row">
		    	<div class="form-group col-md-8">
					<label class="form-control-label">Google META Tag Verifier</label>
					<textarea name="google_verifier" class="form-control" id="" rows="3"><?=$aSiteConfig['google_verifier']?></textarea>
        		</div>

				<div class="col-md-1">
					<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
						title="Here you can paste in the META tag used by Google Webmaster Tools to verify your wesite. You can read more about Google Webmaster Tools and sign up at www.google.com/webmasters/tools">
						<?=assets_img('icon_help.gif')?>
					</a>
				</div>
			</div>

			<hr class="hr">

			<label for="settings_text_sitemap" id="l_settings_text_sitemap">Text Site Map URL</label>
			<p><small>This is the path to the sitemap.txt file for the website which you can use with Google Webmaster Tools.</small></p>
			<p><a id="settings_txt_sitemap" class="sitemap_links" href="<?=$this->config->item('site_url')?>sitemap.txt" target="_blank"><?=$this->config->item('site_url')?>sitemap.txt</a></p>

			<label for="settings_xml_sitemap" id="l_settings_xml_sitemap">XML Site Map URL</label>
			<p><small>This is the path to the sitemap.xml file for the website which you can use with Google Webmaster Tools.</small></p>
			<p><a id="settings_xml_sitemap" class="sitemap_links" href="<?=$this->config->item('site_url')?>sitemap.xml" target="_blank"><?=$this->config->item('site_url')?>sitemap.xml</a></p>

			<hr class="hr">
        
            <span class="input-group-btn"><button href="" value="Update" name="update" type="submit" id="google_settings_update" class="btn btn-activ btn-form">UPDATE</button></span>

		<?=form_close();?>
	</div>
</div>
