<?php

 /*
 * Project:	  Activ CMS Version 5
 * File:	  reset_website_view.php
 * Author:    Activ Developers
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 */

?>

<div class="activ_box col-md-9">

	<?=$aSubnav?>

	<p class="system_h1">Reset Website</p>
	<p class="intro_desc">This section allows you to reset the website..</p>


</div>
