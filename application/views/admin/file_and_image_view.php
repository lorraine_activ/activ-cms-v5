<?php

 /*
 * Project:	  Activ CMS Version 5
 * File:	  file_and_image_view.php
 * Author:    Activ Developers
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 */

?>

<div class="activ_box col-md-11">

	<p class="system_h1">File and Image Manager</p>
	<p class="intro_desc">This section allows you to manage your files and images</p>

	<iframe class="filemanager_iframe" src="<?=$this->config->item('site_url')?>assets/js/plugins/filemanager/dialog.php?type=0"></iframe>

</div>
