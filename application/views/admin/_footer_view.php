<?php
 /*
 * Project:	  Activ CMS Version 5
 * File:	  admin/_footer_view.php 
 * Author:    Activ Developers
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 * Renders the global footer
 */

?>

	</div><!--End div class="system_content"-->
</div><!--End div id="controller"-->

	<!--TODO: Load all JS on the footer-->

	<!--Load JS-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


	<!-- Latest compiled and minified bootstrap select JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

	<!--Load Bootstrap 3.3.7 CDN-->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<?if($isFancyBox){?>
	<!--Load FancyBox-->
	<script src="<?=base_url()?>assets/js/plugins/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
	<?}?>

	<?if($isColorPicker){?>
	<!--Load Colorbox-->
	<script src="<?=base_url()?>assets/js/plugins/colorpicker/js/colorpicker.js"></script>
	<?}?>

	<?if($isEditArea){?>
	<!--Load Edit Area-->
	<script language="Javascript" type="text/javascript" src="<?=base_url()?>assets/js/plugins/edit_area/edit_area_full.js"></script>
	<?}?>

	<?if($isSortable){?>
	<!--Load Sortable Plugin-->
	<script src="<?=base_url();?>assets/js/plugins/sortable/jquery-sortable.js"></script>
	<?}?>

	<!--Load Admin JS -->
	<script src="<?=base_url();?>assets/js/admin/common.js"></script>

	<?if ($aJsFiles){?>
		<script src="<?=base_url();?>assets/js/<?=$aJsFiles?>.js"></script>
		<!--<?=assets_js($aJsFiles)?>-->
	<?}?>

</body>
</html>
