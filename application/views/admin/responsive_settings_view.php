<?php

 /*
 * Project:	  Activ CMS Version 5
 * File:	  responsive_settings_view.php
 * Author:    Activ Developers
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 */

?>

<div class="activ_box col-md-9">

	<?=$aSubnav?>

	<p class="system_h1">Activ Responsive Settings</p>
	<p class="intro_desc">This section is for responsive settings. You can enable and disable responsive feature.</p>


	<div class="responsive_settings_form">

		<?if ($sSuccess){?>
		<div id="formSuccess">
		  <?=$sSuccess?>
		</div>
		<?}?>

		<?if ($sError){?>
		<div id="formError">
		  <?=$sError?>
		</div>
		<?}?>


		<form action="<?=$this->config->item('site_url')?>admin/responsive_settings/" method="post">


		</form>
	</div>
</div>
