<?php
 /*
 * Project:	  Activ CMS Version 5
 * File:	  admin/login_view.php 
 * Author:    Activ Developers
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 * Renders login form
 */

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=$this->config->item('language')?>">

	<head>

		<title>Activ Administration Control Panel</title>
	
		<meta http-equiv="Content-Type" content="text/html; <?=$this->config->item('charset')?>" />
		<meta http-equiv="content-language" content="<?=$this->config->item('language')?>" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<base href="<?=$this->config->item('site_url')?>admin/" />

		<!--Favicon-->
		<link rel="shortcut icon" href="<?=$this->config->item('site_url')?>assets/images/favicon.ico" type="image/x-icon">

		<!--Load Bootstrap 3.3.7 CDN-->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!--Load Fontawesome-->
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

		<!--Load Font-->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

		<!-- Load Admin CSS -->
		<?=assets_css('admin.css', array("title" => "Admin Style"));?>

	</head> 

<body>

	<!-- LOGIN FORM -->
	<div class="text-center" id="login">
		<div class="logo"><a href="http://www.activwebdesign.com/" target="_blank"><?=assets_img('activ_logo.png', array('alt' => 'Activ Web Design'))?></a></div>

		<h1>Control Panel Login</h1>
	  	<p>Please enter your username and password below to enter the Administration Control Panel.</p>
	  	<?php if($this->session->flashdata('msg')): ?>
		    <p class="text-success"><?= $this->session->flashdata('msg'); ?></p>
		<?php endif; ?>
		<!-- Main Form -->
		<div class="login-form-1">
			<?=form_open('admin/ajax_admin/login');?>
				<div class="login-form-main-message"></div>
				<div class="main-login-form">
					<div class="login-group">
						<div class="form-group">
							<label for="lg_username" class="sr-only">Username</label>
							<input type="text" class="form-control" id="lg_username" name="lg_username" placeholder="Username">
						</div>
						<div class="form-group">
							<label for="lg_password" class="sr-only">Password</label>
							<input type="password" class="form-control" id="lg_password" name="lg_password" placeholder="Password">
						</div>

						<input type="hidden" name="action" value="process-login">
						<a href="<?=base_url()?>admin/login/forgot_password" class="text-primary"><u> Forgot password? </u> </a>
					</div>
					<button type="submit" class="login-button"><i class="fa fa-chevron-right"></i></button>
				</div>
			<?=form_close();?>
		</div>
		<!-- end:Main Form -->
	</div>


	<!--Load JS-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>


	<!--Load Bootstrap 3.3.7 CDN-->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<?=assets_js('admin/login')?>

</body>
</html>
