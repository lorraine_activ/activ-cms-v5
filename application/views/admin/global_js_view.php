<?php

 /*
 * Project:	  Activ CMS Version 5
 * File:	  global_css_view.php
 * Author:    Marc Lorraine Jose
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 */

?>

<div class="activ_box col-md-9">

	<p class="system_h1">Global Javascripts</p>
	<p class="intro_desc">This section allows you to add your own javascript codes. The JS code will be used throughout the site. <strong>NO NEED</strong> to add the opening and closing script tags</p>


	<div class="global_js_form">

		
		<?if ($sSuccess){?>
		<div id="formSuccess">
		  <?=$sSuccess?>
		</div>
		<?}?>

		<?if ($sError){?>
		<div id="formError">
		  <?=$sError?>
		</div>
		<?}?>


		<?=form_open('admin/global_js',array("id"=>'form_global_js'));?>
			<div class="row">
		    	<div class="form-group col-md-12">
					<label for="">Advance Javascripts</label>
					<textarea name="advance_js" class="form-control" id="advance_js" rows="20"><?=$aGlobalJs?></textarea>
		  		</div>
			</div>

			<hr class="hr">
        
            <span class="input-group-btn"><button href="" value="Update" name="update" type="submit" id="global_js_update" class="btn btn-activ btn-form">UPDATE</button></span>
		<?=form_close();?>

	</div>

</div>
