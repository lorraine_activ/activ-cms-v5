<?php

 /*
 * Project:	  Activ CMS Version 5
 * File:	  site_settings_view.php
 * Author:    Activ Developers
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 */

?>

<div class="activ_box col-md-9">

	<p class="system_h1">Activ Site Settings</p>
	<p class="intro_desc">This section allows you to setup site requirements that are neccessary for the site to function properly.</p>

	<div class="site_settings_form">

		
		<?if ($sSuccess){?>
		<div id="formSuccess">
		  <?=$sSuccess?>
		</div>
		<?}?>

		<?if ($sError){?>
		<div id="formError">
		  <?=$sError?>
		</div>
		<?}?>

		<?=form_open('admin/site_settings/update_active_settings',array('id'=>'form_site_settings','name'=>'form_site_settings'));?>

			<div class="row site_is_online">
				<div class="form-check col-md-7">
				  <p class="text_bold">Site is Online</p>
				  <label class="form-check-label text_normal">
					<input name="site_online" class="form-check-input" type="checkbox" value="1" <?=($aSiteConfig['site_online'] == 1 ? 'checked=checked' : '')?>>
					Site is <i>Activ</i> and online
				  </label>
				</div>
			</div>

			<hr class="hr"></hr>

			<div class="row">
		    	<div class="form-group col-md-7">
					<label class="form-control-label">Site Name</label>
					<input type="text" class="form-control" name="site_name" required="" id="" value="<?=$aSiteConfig['site_name']?>">
		    	</div>

				<div class="col-md-1">
					<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
						title="Here you can enter a site name which will appear throughout the website. Typically this will be the name of a company, organisation or individual.">
						<?=assets_img('icon_help.gif')?>
					</a>
				</div>
			</div>

			<div class="row">
		    	<div class="form-group col-md-7">
					<label class="form-control-label">Site Domain</label>
					<input type="text" class="form-control" name="site_domain" required="" id="" value="<?=$aSiteConfig['site_domain']?>">
		    	</div>

				<div class="col-md-1">
					<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
						title="Enter here the domain name of the website without the 'www.' part. (i.e. somedomain.com)">
						<?=assets_img('icon_help.gif')?>
					</a>
				</div>
        	</div>


			<div class="row">
		    	<div class="form-group col-md-7">
					<label class="form-control-label">Site URL</label>
					<input type="text" class="form-control" name="site_url" required="" id="" value="<?=$aSiteConfig['site_url']?>">
		    	</div>

				<div class="col-md-1">
					<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
						title="Enter here the full url of the website. (i.e. http://www.somedomain.com/). Please remember to enter a trailing slash on the end.">
						<?=assets_img('icon_help.gif')?>
					</a>
				</div>
        	</div>

			<div class="row">
		    	<div class="form-group col-md-7">
					<label class="form-control-label">Site Favicon</label>
					<input type="text" class="form-control file_manager_input" name="site_favicon" id="site_favicon" value="<?=$aSiteConfig['site_favicon']?>">
					<a class="open_filemanager" href="<?=base_url()?>../assets/js/plugins/filemanager/dialog.php?type=1&field_id=site_favicon">
						<i class="fa fa-file-image-o" aria-hidden="true"></i>
					</a>
		    	</div>

				<div class="col-md-1">
					<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
						title="Here you can change the default favicon for the website.">
						<?=assets_img('icon_help.gif')?>
					</a>
				</div>
        	</div>	

			<hr class="hr"></hr>

			<div class="row">
		    	<div class="form-group col-md-7">
					<label class="form-control-label">Site Title</label>
					<input type="text" class="form-control" name="site_title" required="" id="" value="<?=$aSiteConfig['site_title']?>">
        		</div>

				<div class="col-md-1">
					<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
						title="Enter here a title for the website which will appear on all pages of the website by default. You can overide this by entering individual page titles in the 'Pages Manager'.">
						<?=assets_img('icon_help.gif')?>
					</a>
				</div>
			</div>

			<div class="row">
		    	<div class="form-group col-md-7">
					<label for="">Meta Description</label>
					<textarea name="site_metadesc" class="form-control" id="" rows="3"><?=$aSiteConfig['site_metadesc']?></textarea>
		  		</div>

				<div class="col-md-1">
					<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
						title="Enter here a meta description which will appear on all pages of the website by default. You can overide this by entering meta descriptions for individual pages in the 'Pages Manager'.">
						<?=assets_img('icon_help.gif')?>
					</a>
				</div>
			</div>

			<div class="row">
		    	<div class="form-group col-md-7">
					<label for="">Meta Keywords</label>
					<textarea name="site_metakeys" class="form-control" id="" rows="3"><?=$aSiteConfig['site_metakeys']?></textarea>
		  		</div>

				<div class="col-md-1">
					<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
						title="Enter here a list of keywords separated by commas which will appear on all pages of the website by default. You can overide this by entering separate meta keyword lists for individual pages in the 'Pages Manager'.">
						<?=assets_img('icon_help.gif')?>
					</a>
				</div>
			</div>

			<hr class="hr"></hr>

			<div class="row">
		    	<div class="form-group col-md-7">
					<label class="form-control-label">Contact Email Address</label>
					<input class="form-control" name="contact_email" type="email" data-fv-emailaddress-message="Invalid email address" value="<?=$aSiteConfig['contact_email']?>"/>
        		</div>

				<div class="col-md-1">
					<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
						title="Enter a contact email address for the site administrator here. This is to receive notifications from the website when visiters register, sign up to the newsletter, comment on news articles and complete forms.">
						<?=assets_img('icon_help.gif')?>
					</a>
				</div>
        	</div>

			<hr class="hr"></hr>
        
            <span class="input-group-btn"><button href="" value="Update" name="update" type="submit" id="site_settings_update" class="btn btn-activ btn-form">UPDATE</button></span>

     	<?=form_close();?>

	</div>

</div>
