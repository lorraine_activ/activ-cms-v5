<?php
 /*
 * Project:   Activ CMS Version 5
 * File:    admin/login_view.php 
 * Author:    Activ Developers
 * Date     December 2017
 * @copyright 2017 Activ Web Design 
 * Renders login form
 */

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=$this->config->item('language')?>">

  <head>

    <title>Activ Administration Control Panel</title>
  
    <meta http-equiv="Content-Type" content="text/html; <?=$this->config->item('charset')?>" />
    <meta http-equiv="content-language" content="<?=$this->config->item('language')?>" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <base href="<?=$this->config->item('site_url')?>admin/" />

    <!--Favicon-->
    <link rel="shortcut icon" href="<?=$this->config->item('site_url')?>assets/images/favicon.ico" type="image/x-icon">

    <!--Load Bootstrap 3.3.7 CDN-->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!--Load Fontawesome-->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!--Load Font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    <!-- Load Admin CSS -->
    <?=assets_css('admin.css', array("title" => "Admin Style"));?>

  </head> 

<body>

  <!-- LOGIN FORM -->
  <div class="text-center" id="login">
    <div class="logo"><a href="http://www.activwebdesign.com/" target="_blank"><?=assets_img('activ_logo.png', array('alt' => 'Activ Web Design'))?></a></div>

    <h1>Control Panel Login</h1>
      <p>Password Recovery.</p>
    <?php if($this->session->flashdata('msg')): ?>
        <p class="text-danger"><?= $this->session->flashdata('msg'); ?></p>
    <?php endif; ?>
    <!-- Main Form -->
    <div class="login-form-1">
        <div class="page-container container-fluid">
            <div class="login-wrapper">
                <hr/>
                <div id="errorMessage"></div>
                <div class="panel panel-default">
                    <div class="panel-heading"><h6 class="panel-title"><i class="fa fa-key"></i> Reset Password</h6></div>
                    <div class="panel-body">
                        <?=form_open('admin/forgotpassword/activate_new_password',array('id'=>'reset_password_form'));?>
                            <div class="form-group">
                                <label for="Password">Type your new password below:</label>
                                <input id="password" type="password" name="password" class="form-control" placeholder="new password" required>
                            </div>
                            <div class="form-group">
                                <label for="Password Confirmation">Re-type your new password:</label>
                                <input type="password" name="passconf" id="passconf"
                                       class="form-control" placeholder=" new password">
                            </div>
                            <input type="hidden"  name = 'user_id' id="user_id" value="<?=$id?>" />
                            <div class="clearfix"></div>
                            <div class="form-actions text-right">
                                <button type="submit" class="btn btn-primary">Save New Password</button>
                            </div>
                        <?=form_close();?>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- end:Main Form -->
  </div>


  <!--Load JS-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>


  <!--Load Bootstrap 3.3.7 CDN-->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <?=assets_js('admin/login')?>
</body>
</html>
