<?php

 /*
 * Project:	  Activ CMS Version 5
 * File:	  site_settings_view.php
 * Author:    Activ Developers
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 */

?>

<div class="activ_box col-md-9">

	<?=$aSubnav?>

	<p class="system_h1">Activ Users</p>
	<p class="intro_desc">In this section you can create, amend and remove user accounts within the website and admin cpanel. You can also export users to csv file.</p>

	<div class="users_form">

		<?if($aUsers){?>
			<p><i class="fa fa-download" aria-hidden="true"></i> Click here to <strong>export</strong> users to CSV</p>
		<?}else{?>
			<p>There are no current users registered.</p>
		<?}?>

		<hr class="hr">	

		<?if ($sSuccess){?>
		<div id="formSuccess">
		  <?=$sSuccess?>
		</div>
		<?}?>

		<?if ($sError){?>
		<div id="formError">
		  <?=$sError?>
		</div>
		<?}?>
	

		<?=form_open('admin/users',array('id'=>'form_users'));?>
			<div class="row">
		    	<div class="form-group col-md-8">
					<label for="">List of Current Users</label>
					<select class="selectpicker" data-live-search="true" name="id" id="user_id">
						<option value="">** Create New User **</option>

						<optgroup id="not_approved" label="Users waiting to be approved">
							<?foreach ($aUsers as $user){?>
								<?if($user['is_approved'] == 0 && $user['role'] == 4){?>
									<option <?=($user['id'] == $sId ? 'selected=true' : '')?> value="<?=$user['id']?>" data-tokens="<?=$user['fullname']?>" title="<?=$user['fullname']?>"><?=$user['fullname']?></option>
								<?}?>
							<?}?>
						</optgroup>

						<optgroup id="approved" label="Approved Users">
							<?foreach ($aUsers as $user){?>
								<?if($user['is_approved'] == 1 && $user['role'] == 4){?>
									<option <?=($user['id'] == $sId ? 'selected=true' : '')?> value="<?=$user['id']?>" data-tokens="<?=$user['fullname']?>" title="<?=$user['fullname']?>"><?=$user['fullname']?></option>
								<?}?>
							<?}?>
						</optgroup>

						<optgroup id="admin" label="Admin Users">
							<?foreach ($aUsers as $user){?>
								<?if($user['role'] < 4){?>
									<option <?=($user['id'] == $sId ? 'selected=true' : '')?> value="<?=$user['id']?>" data-tokens="<?=$user['fullname']?>" title="<?=$user['fullname']?>"><?=$user['fullname']?></option>
								<?}?>
							<?}?>
						</optgroup>
					</select>
		  		</div>

				<div class="col-md-1">
					<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
						title="Here you can paste in the JavaScript tracking code provided by Google Analytics. You can read more about Google Analytics and sign up at http://www.google.com/analytics">
						<?=assets_img('icon_help.gif')?>
					</a>
				</div>
			</div>

			<div class="row">
		    	<div class="form-group col-md-8">
					<label for="">Select User Role</label>
					<select class="selectpicker" title="Select User Role..." name="role" id="user_role">
					  <option <?=($aUser['role'] == 2 || set_value('role') == 2 ? 'selected=true' : '')?> value="2">Franchisee (Admin User)</option>
					  <option <?=($aUser['role'] == 3 || set_value('role') == 3  ? 'selected=true' : '')?> value="3">Client (Admin User)</option>
					  <option <?=($aUser['role'] == 4 || set_value('role') == 4  ? 'selected=true' : '')?> value="4">Website Member</option>
					</select>
		  		</div>

				<div class="col-md-1">
					<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
						title="Here you can paste in the JavaScript tracking code provided by Google Analytics. You can read more about Google Analytics and sign up at http://www.google.com/analytics">
						<?=assets_img('icon_help.gif')?>
					</a>
				</div>
			</div>


			<div class="row">
		    	<div class="form-group col-md-8">
					<label for="">First Name</label>
					<input type="text" class="form-control" name="firstname" required="" id="firstname" value="<?=($aUser['firstname'] ? form_prep($aUser['firstname']) : set_value('firstname'))?>">
		  		</div>

				<div class="col-md-1">
					<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
						title="Here you can paste in the JavaScript tracking code provided by Google Analytics. You can read more about Google Analytics and sign up at http://www.google.com/analytics">
						<?=assets_img('icon_help.gif')?>
					</a>
				</div>
			</div>

			<div class="row">
		    	<div class="form-group col-md-8">
					<label for="">Last Name</label>
					<input type="text" class="form-control" name="lastname" required="" id="lastname" value="<?=($aUser['lastname'] ? form_prep($aUser['lastname']) : set_value('lastname'))?>">
		  		</div>

				<div class="col-md-1">
					<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
						title="Here you can paste in the JavaScript tracking code provided by Google Analytics. You can read more about Google Analytics and sign up at http://www.google.com/analytics">
						<?=assets_img('icon_help.gif')?>
					</a>
				</div>
			</div>

			<div class="row">
		    	<div class="form-group col-md-8">
					<label class="form-control-label">Email Address</label>
					<input class="form-control" name="email" type="email" id="email" value="<?=($aUser['email'] ? form_prep($aUser['email']) : set_value('email'))?>"/>
        		</div>

				<div class="col-md-1">
					<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
						title="Enter a contact email address for the site administrator here. This is to receive notifications from the website when visiters register, sign up to the newsletter, comment on news articles and complete forms.">
						<?=assets_img('icon_help.gif')?>
					</a>
				</div>
        	</div>

			<div class="row">
		    	<div class="form-group col-md-8">
					<label for="">Password</label>
					<input type="password" class="form-control" name="password" id="user_password">
		  		</div>

				<div class="col-md-1">
					<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
						title="Here you can paste in the JavaScript tracking code provided by Google Analytics. You can read more about Google Analytics and sign up at http://www.google.com/analytics">
						<?=assets_img('icon_help.gif')?>
					</a>
				</div>
			</div>

			<div class="row">
		    	<div class="form-group col-md-8">
					<label for="">Confirm Password</label>
					<input type="password" class="form-control" name="confirm_password" id="user_confirm">
		  		</div>

				<div class="col-md-1">
					<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
						title="Here you can paste in the JavaScript tracking code provided by Google Analytics. You can read more about Google Analytics and sign up at http://www.google.com/analytics">
						<?=assets_img('icon_help.gif')?>
					</a>
				</div>
			</div>

			<div class="is_approved_div" style="display:<?if ($aUser['role'] == 4 || set_value('role') == 4 || $aUser['role'] == ''){?>block;<?}else{?>none;<?}?>">

				<hr class="hr">

				<div class="row site_is_online">
					<div class="form-check col-md-7">
					  <p class="text_bold">Approve Member</p>
					  <label class="form-check-label text_normal">
						<input id="is_approved" class="form-check-input" name="is_approved" type="checkbox" value="1" <?=($aUser['is_approved'] == 1 || set_value('is_approved') ? 'checked=checked' : '')?>>
						This member is <strong>approved</strong>.
					  </label>
					</div>
				</div>
			</div>
			

			<hr class="hr">

			<!--hidden values-->
        	<input type="hidden" name="action" id="action" value="">
			<div class="delete_dialog notice" title="Delete User">
				Are you sure you want to delete this user?
			</div>

            <span class="input-group-btn">
				<?if ($sId){?>
				<button href="" value="Update" name="update" type="submit" id="users_update" class="btn btn-activ btn-form">UPDATE</button>
				<button href="" value="Delete" name="delete" type="submit" id="users_delete" class="btn btn-activ btn-form">DELETE</button>
				<?}else{?>
				<button href="" value="Add" name="add" type="submit" id="users_add" class="btn btn-activ btn-form">ADD</button>
				<?}?>
			</span>

     	<?=form_close();?>

	</div>

</div>
