<?php

 /*
 * Project:	  Activ CMS Version 5
 * File:	  global_css_view.php
 * Author:    Marc Lorraine Jose
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 */

?>

<div class="activ_box col-md-9">

	<p class="system_h1">Global CSS Styles</p>
	<p class="intro_desc">This section allows you to add your own css codes. The css code will be used throughout the site.</p>


	<div class="global_css_form">

		
		<?if ($sSuccess){?>
		<div id="formSuccess">
		  <?=$sSuccess?>
		</div>
		<?}?>

		<?if ($sError){?>
		<div id="formError">
		  <?=$sError?>
		</div>
		<?}?>


		<?=form_open('admin/global_css',array('id'=>'form_global_css'));?>	
			<div class="row">
		    	<div class="form-group col-md-12">
					<label for="">Advance CSS Styles</label>
					<textarea name="advance_css" class="form-control" id="advance_css" rows="20"><?=$aGlobalCss?></textarea>
		  		</div>
			</div>

			<hr class="hr">
        
            <span class="input-group-btn"><button href="" value="Update" name="update" type="submit" id="global_css_update" class="btn btn-activ btn-form">UPDATE</button></span>


		<?=form_close();?>

	</div>

</div>
