<?php

 /*
 * Project:	  Activ CMS Version 5
 * File:	  reset_website_view.php
 * Author:    Marc Lorraine Jose
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 */

?>

<div class="activ_box col-md-9">

	<p class="system_h1">Global Settings</p>
	<p class="intro_desc">This section allows you to update global settings for content, header and footer.</p>

	<div class="global_settings_form">	

		<?if ($sSuccess){?>
		<div id="formSuccess">
		  <?=$sSuccess?>
		</div>
		<?}?>

		<?if ($sError){?>
		<div id="formError">
		  <?=$sError?>
		</div>
		<?}?>
	

		<?=form_open('admin/global_settings',array('id'=>'form_global_settings'));?>
		  	<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#layout_settings">Layout Settings</a></li>
				<li><a data-toggle="tab" href="#content_settings">Content Settings</a></li>
				<li><a data-toggle="tab" href="#header_settings">Header Settings</a></li>
				<li><a data-toggle="tab" href="#footer_settings">Footer Settings</a></li>
		  	</ul>

 			<div class="tab-content">

				<!--Content Settings-->
				<div id="layout_settings" class="tab-pane fade in active">

					<div class="row site_width">
						<div class="form-check col-md-7">
						  <p class="text_bold">Website Width</p>
			
						
						  <div>
								<label class="form-radio-label  text_normal">
									<input name="website_width_type" class="form-radio-input width_selection" type="radio" value="px" <?=($aSiteConfig['website_width_type'] == 'px' ? 'checked=checked' : '')?>>Set pixel width
							  	</label>

								<div class="wesite_pixel_width_div" <?=($aSiteConfig['website_width_type'] == 'px' ? "style='display:block;'" : 'style="display:none;"')?>>

							  		<div class="demo">
						  				<div id="wesite_pixel_width_slider"></div>
									</div>

									<div class="slider_input_fields">
										<input type="hidden" name="current_px_value" value="<?=$aSiteConfig['website_px_width']?>" id="current_px_value" />
										<input type="text" class="slider_input form-control" name="website_px_width" id="website_px_width" readonly="readonly" value="<?=$aSiteConfig['website_px_width']?>%">
									</div>

								</div>
						  </div>

						  <div>
						  		<label class="form-radio-label  text_normal">
								<input name="website_width_type" class="form-radio-input width_selection" type="radio" value="%" <?=($aSiteConfig['website_width_type'] == '%' ? 'checked=checked' : '')?>>Set percentage width
						  		</label>

								<div class="wesite_percent_width_div" <?=($aSiteConfig['website_width_type'] == '%' ? "style='display:block;'" : 'style="display:none;"')?>>
							  		<div class="demo">
						  				<div id="wesite_percent_width_slider"></div>
									</div>

									<div class="slider_input_fields">
										<input type="hidden" name="current_percent_value" value="<?=$aSiteConfig['website_percent_width']?>" id="current_percent_value" />
										<input type="text" class="slider_input form-control" name="website_percent_width" id="website_percent_width" readonly="readonly" value="<?=$aSiteConfig['website_percent_width']?>%">
									</div>
								</div>
						  </div>

						</div>
					</div>


				</div>

				<!--Content Settings-->
				<div id="content_settings" class="tab-pane fade">

					<div class="row">
						<div class="form-group col-md-8">
							<label for="">Default Font Style</label>
							<select class="selectpicker" data-live-search="true" title="Select Default Font..." name="default_font_style" id="default_font_style">
								<?foreach ($aWebFonts as $fonts){?>
									<option <?=($fonts['id'] == $aSiteConfig['default_font_style'] ? 'selected=true' : '')?> value="<?=$fonts['id']?>" data-tokens="<?=$fonts['name']?>" title="<?=$fonts['name']?>"><?=$fonts['name']?></option>
								<?}?>
							</select>
				  		</div>

						<div class="col-md-1">
							<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
								title="This allows to categorized users by either admin or website user.">
								<?=assets_img('icon_help.gif')?>
							</a>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-8">
							<label class="form-control-label default_font_label">Default Font Size</label>

						  	<div class="demo">
					  			<div id="default_font_size_slider"></div>
							</div>

							<div class="slider_input_fields">
								<input type="hidden" name="current_default_font_size" value="<?=$aSiteConfig['default_font_size']?>" id="current_default_font_size" />
								<input type="text" class="form-control slider_input" name="default_font_size" id="default_font_size" readonly="readonly" value="<?=$aSiteConfig['default_font_size']?>%">
							</div>

						</div>

						<div class="col-md-1">
							<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
								title="Enter here the domain name of the website without the 'www.' part. (i.e. somedomain.com)">
								<?=assets_img('icon_help.gif')?>
							</a>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-4">
							<label class="form-control-label">Default Font Colour</label>
							<input type="text" class="form-control" name="default_font_colour" required="" id="default_font_colour" value="<?=$aSiteConfig['default_font_colour']?>">
						</div>

						<div class="col-md-1">
							<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
								title="Enter here the domain name of the website without the 'www.' part. (i.e. somedomain.com)">
								<?=assets_img('icon_help.gif')?>
							</a>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-4">
							<label class="form-control-label">Default Link Colour</label>
							<input type="text" class="form-control" name="default_link_colour" required="" id="default_link_colour" value="<?=$aSiteConfig['default_link_colour']?>">
						</div>

						<div class="col-md-1">
							<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
								title="Enter here the domain name of the website without the 'www.' part. (i.e. somedomain.com)">
								<?=assets_img('icon_help.gif')?>
							</a>
						</div>
					</div>

				</div>

				<!--Header Settings-->
				<div id="header_settings" class="tab-pane fade">

					<div class="row">
						<div class="form-group col-md-7">
							<label class="form-control-label">Header Logo</label>
							<input type="text" class="form-control file_manager_input" name="site_logo" required="" id="site_logo" value="<?=$aSiteConfig['site_logo']?>">
							<a class="open_filemanager" href="<?=$this->config->item('site_url')?>assets/js/plugins/filemanager/dialog.php?type=1&field_id=site_logo">
								<i class="fa fa-file-image-o" aria-hidden="true"></i>
							</a>
						</div>

						<div class="col-md-1">
							<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
								title="Here you can change the default favicon for the website.">
								<?=assets_img('icon_help.gif')?>
							</a>
						</div>
					</div>	

					<div class="row cookie_notice">
						<div class="form-check col-md-7">
						  <p class="text_bold">Cookie Notice on Header</p>

						  <label class="form-check-label text_normal">
							<input name="show_cookie_on_header" class="form-check-input" type="checkbox" value="1" <?=($aSiteConfig['show_cookie_on_header'] == 1 ? 'checked=checked' : '')?>>
							Show cookie notice on header
						  </label>
						</div>
					</div>

				</div>

				<!--Footer Settings-->
				<div id="footer_settings" class="tab-pane fade">

					<div class="row show_sitemap_on_footer">
						<div class="form-check col-md-7">
						  <p class="text_bold">Sitemap on Footer</p>

						  <label class="form-check-label text_normal">
							<input name="show_sitemap_on_footer" class="form-check-input" type="checkbox" value="<?=($aSiteConfig['show_sitemap_on_footer'] == 1 ? 1 : 0)?>" <?=($aSiteConfig['show_sitemap_on_footer'] == 1 ? 'checked=checked' : '')?>>
							Show sitemap on footer
						  </label>
						</div>
					</div>

					<hr class="hr">

					<div class="row">
						<div class="form-group col-md-8">
							<label class="form-control-label">Footer Company Name</label>
							<input type="text" class="form-control" name="footer_company_name" required="" id="footer_company_name" value="<?=$aSiteConfig['footer_company_name']?>">
						</div>

						<div class="col-md-1">
							<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
								title="Enter here the domain name of the website without the 'www.' part. (i.e. somedomain.com)">
								<?=assets_img('icon_help.gif')?>
							</a>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-8">
							<label class="form-control-label">Footer Company URL</label>
							<input type="text" class="form-control" name="footer_company_url" required="" id="footer_company_url" value="<?=$aSiteConfig['footer_company_url']?>">
						</div>

						<div class="col-md-1">
							<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
								title="Enter here the domain name of the website without the 'www.' part. (i.e. somedomain.com)">
								<?=assets_img('icon_help.gif')?>
							</a>
						</div>
					</div>

				</div>

				</div>

			</div>
        
			<hr class="hr">

            <span class="input-group-btn"><button href="" value="Update" name="update" type="submit" id="global_settings_update" class="btn btn-activ btn-form">UPDATE</button></span>

	
		<?=form_close();?>


</div>
