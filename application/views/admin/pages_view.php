<?php defined('BASEPATH') OR exit('No direct script access allowed');

 /*
 * Project:	  Activ CMS Version 5
 * File:	  page_view.php
 * Author:    Marc Lorraine Jose
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 */

?>

<div class="activ_box col-md-9">


	<p class="system_h1">Pages</p>
	<p class="intro_desc">This section allows you to add, edit, delete and sort pages. You can also add your custom page URL</p>


	<div class="pages_form">

		<p><a data-toggle="modal" data-target="#sortpages_modal" id="sort_pages"><i class="fa fa-list" aria-hidden="true"></i> Click here to <strong>sort</strong> pages.</a></p>

		<hr class="hr">	

		<?if ($sSuccess){?>
		<div id="formSuccess">
		  <?=$sSuccess?>
		</div>
		<?}?>

		<?if ($sError){?>
		<div id="formError">
		  <?=$sError?>
		</div>
		<?}?>
	
		<?= form_open("admin/pages/add_user_page",array('name'=>'form_pages','id'=>'form_pages'));?>
					<div class="row">
						<div class="form-group col-md-7">
							<label for="">List of Pages</label>
							<select class="selectpicker" data-live-search="true" name="id" id="page_id" data-style="activ-select">
								<option value="">** Create New Page **</option>

								<optgroup id="fixed_pages" label="Fixed Pages">
									<?foreach ($aPages[0] as $page){?>
										<?if($page['is_fixed'] == 1){?>
											<!--1st Level-->
											<option <?=($page['id'] == $sId ? 'selected=true' : '')?> value="<?=$page['id']?>" data-tokens="<?=$page['name']?>" title="<?=$page['name']?>"><?=$page['name']?></option>

											<!--2nd Level-->
											<?if($aPages[$page['id']]){?>
												<?foreach ($aPages[$page['id']] as $subpage2){?>
													<option data-depth="<?=$subpage2['depth']?>" <?=($subpage2['id'] == $sId ? 'selected=true' : '')?> value="<?=$subpage2['id']?>" data-tokens="<?=$subpage2['name']?>" title="<?=$subpage2['name']?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$subpage2['name']?></option>

													<!--3rd Level-->
													<?if($aPages[$subpage2['id']]){?>

														<?foreach ($aPages[$subpage2['id']] as $subpage3){?>
															<option data-depth="<?=$subpage3['depth']?>" <?=($subpage3['id'] == $sId ? 'selected=true' : '')?> value="<?=$subpage3['id']?>" data-tokens="<?=$subpage3['name']?>" title="<?=$subpage3['name']?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$subpage3['name']?></option>


															<!--4th Level-->
															<?if($aPages[$subpage3['id']]){?>

																<?foreach ($aPages[$subpage3['id']] as $subpage4){?>
																	<option data-depth="<?=$subpage4['depth']?>" <?=($subpage4['id'] == $sId ? 'selected=true' : '')?> value="<?=$subpage4['id']?>" data-tokens="<?=$subpage4['name']?>" title="<?=$subpage4['name']?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$subpage4['name']?></option>

																	<!--5th Level-->
																	<?if($aPages[$subpage4['id']]){?>

																		<?foreach ($aPages[$subpage4['id']] as $subpage5){?>
																			<option data-depth="<?=$subpage5['depth']?>" <?=($subpage5['id'] == $sId ? 'selected=true' : '')?> value="<?=$subpage5['id']?>" data-tokens="<?=$subpage5['name']?>" title="<?=$subpage5['name']?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$subpage5['name']?></option>
																		<?}?>

																	<?}?><!--End 5th Level-->

																<?}?>

															<?}?><!--End 4th Level-->


														<?}?>
													<?}?><!--End 3rd Level-->

												<?}?>

											<?}?><!--End 2nd Level-->

										<?}?>
									<?}?><!--End 1st Level-->
								</optgroup>

								<optgroup id="activ_pages" label="Activ Pages">
									<?foreach ($aPages[0] as $page){?>

										<?if($page['is_fixed'] == 0){?>
											<!--1st Level-->
											<option <?=($page['id'] == $sId ? 'selected=true' : '')?> value="<?=$page['id']?>" data-tokens="<?=$page['name']?>" title="<?=$page['name']?>"><?=$page['name']?></option>

											<!--2nd Level-->
											<?if($aPages[$page['id']]){?>
												<?foreach ($aPages[$page['id']] as $subpage2){?>
													<option data-depth="<?=$subpage2['depth']?>" <?=($subpage2['id'] == $sId ? 'selected=true' : '')?> value="<?=$subpage2['id']?>" data-tokens="<?=$subpage2['name']?>" title="<?=$subpage2['name']?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$subpage2['name']?></option>

													<!--3rd Level-->
													<?if($aPages[$subpage2['id']]){?>
														<?foreach ($aPages[$subpage2['id']] as $subpage3){?>
															<option data-depth="<?=$subpage3['depth']?>" <?=($subpage3['id'] == $sId ? 'selected=true' : '')?> value="<?=$subpage3['id']?>" data-tokens="<?=$subpage3['name']?>" title="<?=$subpage3['name']?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$subpage3['name']?></option>

															<!--4th Level-->
															<?if($aPages[$subpage3['id']]){?>

																<?foreach ($aPages[$subpage3['id']] as $subpage4){?>
																	<option data-depth="<?=$subpage4['depth']?>" <?=($subpage4['id'] == $sId ? 'selected=true' : '')?> value="<?=$subpage4['id']?>" data-tokens="<?=$subpage4['name']?>" title="<?=$subpage4['name']?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$subpage4['name']?></option>

																	<!--5th Level-->
																	<?if($aPages[$subpage4['id']]){?>

																		<?foreach ($aPages[$subpage4['id']] as $subpage5){?>
																			<option data-depth="<?=$subpage5['depth']?>" <?=($subpage5['id'] == $sId ? 'selected=true' : '')?> value="<?=$subpage5['id']?>" data-tokens="<?=$subpage5['name']?>" title="<?=$subpage5['name']?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$subpage5['name']?></option>
																		<?}?>

																	<?}?><!--End 5th Level-->


																<?}?>

															<?}?><!--End 4th Level-->

														<?}?>
													<?}?><!--End 3rd Level-->

												<?}?>

											<?}?><!--End 2nd Level-->

										<?}?>
									<?}?><!--End 1st Level-->
								</optgroup>
							</select>
				  		</div>

						<div class="col-md-1">
							<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
								title="Using the drop down menu you can select an existing user whose details you can edit or leave as the default selection to begin adding a new member account. The drop down list is categorized by user roles.">
								<?=assets_img('icon_help.gif')?>
							</a>
						</div>
					</div>

					<!--Page Name-->
					<div class="row">
						<div class="form-group col-md-7">
							<label for="">Page Name</label>
							<input type="text" class="form-control" name="name" required="" id="name" value="<?=($aPage['name'] ? form_prep($aPage['name']) : set_value('name'))?>">
				  		</div>

						<div class="col-md-1">
							<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
								title="This is the firstname of the member.">
								<?=assets_img('icon_help.gif')?>
							</a>
						</div>
					</div>

					<?if ($aPage['is_fixed'] == 0){?>
					<!--Parent Page-->
					<div class="row">
						<div class="form-group col-md-7">
							<label for="">Select Parent Page</label>
							<select class="selectpicker" data-live-search="true" name="parent_id" id="parent_id" data-style="activ-select">
								<option value="0">** Nothing Selected **</option>

								<optgroup id="" label="Select Parent Page">

								<?foreach ($aPages[0] as $page){?>
									<!--1st Level-->
									<?if($page['id'] != $aPage['id']){?>
										<option data-depth="<?=$page['depth']?>" <?=($page['id'] == $aPage['parent_id'] ||  $page['id'] == set_value('parent_id') ? 'selected=true' : '')?> value="<?=$page['id']?>" data-tokens="<?=$page['name']?>" title="<?=$page['name']?>"><?=$page['name']?></option>

										<!--2nd Level-->
										<?if($aPages[$page['id']]){?>
											<?foreach ($aPages[$page['id']] as $subpage2){?>
												<option data-depth="<?=$subpage2['depth']?>" <?=($subpage2['id'] == $aPage['parent_id'] ||  $subpage2['id'] == set_value('parent_id') ? 'selected=true' : '')?> value="<?=$subpage2['id']?>" data-tokens="<?=$subpage2['name']?>" title="<?=$subpage2['name']?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$subpage2['name']?></option>

												<!--3rd Level-->
												<?if($aPages[$subpage2['id']]){?>
													<?foreach ($aPages[$subpage2['id']] as $subpage3){?>
														<option data-depth="<?=$subpage3['depth']?>" <?=($subpage3['id'] == $aPage['parent_id'] ||  $subpage3['id'] == set_value('parent_id') ? 'selected=true' : '')?> value="<?=$subpage3['id']?>" data-tokens="<?=$subpage3['name']?>" title="<?=$subpage3['name']?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$subpage3['name']?></option>

														<!--4th Level-->
														<?if($aPages[$subpage3['id']]){?>

															<?foreach ($aPages[$subpage3['id']] as $subpage4){?>
																<option data-depth="<?=$subpage4['depth']?>" <?=($subpage4['id'] == $aPage['parent_id'] ||  $subpage4['id'] == set_value('parent_id') ? 'selected=true' : '')?> value="<?=$subpage4['id']?>" data-tokens="<?=$subpage4['name']?>" title="<?=$subpage4['name']?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$subpage4['name']?></option>


																<!--5th Level-->
																<?if($aPages[$subpage4['id']]){?>

																	<?foreach ($aPages[$subpage4['id']] as $subpage5){?>
																		<option data-depth="<?=$subpage5['depth']?>" <?=($subpage5['id'] == $aPage['parent_id'] ||  $subpage5['id'] == set_value('parent_id') ? 'selected=true' : '')?> value="<?=$subpage5['id']?>" data-tokens="<?=$subpage5['name']?>" title="<?=$subpage5['name']?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$subpage5['name']?></option>

																	<?}?>

																<?}?><!--End 4th Level-->


															<?}?>

														<?}?><!--End 4th Level-->
													<?}?>
												<?}?><!--End 3rd Level-->

											<?}?>

										<?}?><!--End 2nd Level-->

									<?}?>
								<?}?><!--End 1st Level-->
								</optgroup>						

							</select>
				  		</div>

						<div class="col-md-1">
							<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
								title="Using the drop down menu you can select an existing user whose details you can edit or leave as the default selection to begin adding a new member account. The drop down list is categorized by user roles.">
								<?=assets_img('icon_help.gif')?>
							</a>
						</div>
					</div>
					<?}?>

					<!--Page Settings-->
					<div class="row site_is_online">
						<div class="form-check col-md-7">
						  <p class="text_bold">Page Settings</p>

						  <label class="form-check-label text_normal">
							<input name="is_private" id="is_private" class="form-check-input" type="checkbox" value="1" <?=($aPage['is_private'] == 1 || set_value('is_private') ? 'checked=checked' : '')?>>
							Page is restricted to Members only
						  </label>

						  <label class="form-check-label text_normal">
							<input name="is_hidden_on_nav" id="is_hidden_on_nav" class="form-check-input" type="checkbox" value="1" <?=($aPage['is_hidden_on_nav'] == 1 || set_value('is_hidden_on_nav') ? 'checked=checked' : '')?>>
							Hide this page on the menu navigation.
						  </label>

						  <label class="form-check-label text_normal">
							<input name="hide_main_nav" id="hide_main_nav" class="form-check-input" type="checkbox" value="1" <?=($aPage['hide_main_nav'] == 1 || set_value('hide_main_nav') ? 'checked=checked' : '')?>>
							Hide the menu navigation on this page.
						  </label>

						</div>
					</div>

				
					<?if($aPage['is_fixed'] == 0){?>
					<hr class="hr"></hr>

					<div class="row">
						<div class="form-check col-md-7 page_url_selection">
							<p class="text_bold">Page URL</p>
							<div>
								<label class="form-radio-label  text_normal">
									<input name="type" class="form-radio-input type_selection" type="radio" value="1" <?=($aPage['type'] == '1' || !$aPage ? 'checked=checked' : '')?>>Auto generate URL
								</label>
						
								<?if($aPage['url']){?>
								<div class="auto_page_div" <?=($aPage['type'] == '1' ? "style='display:block;'" : 'style="display:none;"')?>>
									<a href="<?=$this->config->item('site_url')?><?=$aPage['slug']?>" target="_blank"><input type="text" class="form-control" disabled=true id="" value="<?=$this->config->item('site_url')?><?=$aPage['slug']?>"></a>
								</div>
								<?}?>

							</div>

							<div>
								<label class="form-radio-label  text_normal">
									<input name="type" class="form-radio-input type_selection" type="radio" value="2" <?=($aPage['type'] == '2' ? 'checked=checked' : '')?>>Set Custom URL
								</label>

								<div class="custom_page_div" <?=($aPage['type'] == '2' ? "style='display:block;'" : 'style="display:none;"')?>>

									<input type="text" class="form-control" id="" name="page_url" value="<?=($aPage['url'] ? form_prep($aPage['url']) : set_value('url'))?>">

								</div>

							</div>

						</div>

						<div class="col-md-1">
							<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
								title="Enter here a title for the website which will appear on all pages of the website by default. You can overide this by entering individual page titles in the 'Pages Manager'.">
								<?=assets_img('icon_help.gif')?>
							</a>
						</div>
					</div>
					<?}?>


					<hr class="hr"></hr>

					<!--Page Meta Tags-->
					<div class="row">
						<div class="form-group col-md-7">
							<label class="form-control-label">Page Title</label>
							<input type="text" class="form-control" name="title" id="title" value="<?=($aPage['title'] ? form_prep($aPage['title']) : set_value('title'))?>">
						</div>

						<div class="col-md-1">
							<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
								title="Enter here a title for the website which will appear on all pages of the website by default. You can overide this by entering individual page titles in the 'Pages Manager'.">
								<?=assets_img('icon_help.gif')?>
							</a>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-7">
							<label for="">Page Meta Description</label>
							<textarea name="metadesc" class="form-control" id="metadesc" rows="3"><?=($aPage['metadesc'] ? form_prep($aPage['metadesc']) : set_value('metadesc'))?></textarea>
				  		</div>

						<div class="col-md-1">
							<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
								title="Enter here a meta description which will appear on all pages of the website by default. You can overide this by entering meta descriptions for individual pages in the 'Pages Manager'.">
								<?=assets_img('icon_help.gif')?>
							</a>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-7">
							<label for="">Page Meta Keywords</label>
							<textarea name="metakeys" class="form-control" id="metakeys" rows="3"><?=($aPage['metakeys'] ? form_prep($aPage['metakeys']) : set_value('metakeys'))?></textarea>
				  		</div>

						<div class="col-md-1">
							<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
								title="Enter here a list of keywords separated by commas which will appear on all pages of the website by default. You can overide this by entering separate meta keyword lists for individual pages in the 'Pages Manager'.">
								<?=assets_img('icon_help.gif')?>
							</a>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-7">
							<label for="">Page Redirection Script</label>
							<textarea name="redirect_script" class="form-control" id="redirect_script" rows="3"><?=($aPage['redirect_script'] ? form_prep($aPage['redirect_script']) : set_value('redirect_script'))?></textarea>
				  		</div>

						<div class="col-md-1">
							<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
								title="Enter here a list of keywords separated by commas which will appear on all pages of the website by default. You can overide this by entering separate meta keyword lists for individual pages in the 'Pages Manager'.">
								<?=assets_img('icon_help.gif')?>
							</a>
						</div>
					</div>


					<?if($aPage['created_on'] && $aPage['is_fixed'] == 0){?>

					<hr class="hr"></hr>

					<div class="row">
						<div class="form-group col-md-7">
							<label class="form-control-label">Created On</label>
							<input type="text" class="form-control" disabled=true id="" value="<?=$aPage['created_on']?>">
						</div>

						<div class="col-md-1">
							<a href="#" class="activ_tooltip" data-toggle="tooltip" data-placement="top" 
								title="Enter here a title for the website which will appear on all pages of the website by default. You can overide this by entering individual page titles in the 'Pages Manager'.">
								<?=assets_img('icon_help.gif')?>
							</a>
						</div>
					</div>
					<?}?>

					<hr class="hr"></hr>

					<!--hidden values-->
					<input type="hidden" name="action" id="action" value="">
					<div class="delete_dialog notice" title="Delete Page">
						Are you sure you want to delete this page?
					</div>

				    <span class="input-group-btn">
						<?if ($sId){?>
						<button href="" value="Update" name="update" type="submit" id="page_update" class="btn btn-activ btn-form">UPDATE</button>
							<?if($aPage['is_fixed'] == 0){?>
							<button href="" value="Delete" name="delete" type="submit" id="page_delete" class="btn btn-activ btn-form">DELETE</button>
							<?}?>
						<?}else{?>
						<button href="" value="Add" name="add" type="submit" id="page_add" class="btn btn-activ btn-form">ADD</button>
						<?}?>
					</span>



			<!-- Modal -->
			<div class="modal fade" id="sortpages_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">

						<!-- Modal Header -->
						<div class="modal-header">
						    <button type="button" class="close" data-dismiss="modal">
						           <span aria-hidden="true">&times;</span>
						           <span class="sr-only">Close</span>
						    </button>
						    <h4 class="modal-title" id="myModalLabel">
						        Sort Pages By Drag & Drop
						    </h4>
						</div>
						
						<!-- Modal Body -->
						<div class="modal-body">
						<div class="loader-div" style="display: none;"><div class="loader"></div></div>

							<div class="row">
								<div class="form-group col-md-12 box-border">
									<label class="form-control-label">Note that page order is automatically updated.</label>

									<ol class="sort_pages simple_with_animation vertical">
										<!--1st Level-->
										<?foreach ($aPages[0] as $page){?>										
										<li data-id="<?=$page['id']?>" data-name="<?=$page['name']?>"><?=$page['name']?> 

											<ol>
												<!--2nd Level-->
												<?foreach ($aPages[$page['id']] as $subpage2){?>
												<li data-id="<?=$subpage2['id']?>" data-name="<?=$subpage2['name']?>"><?=$subpage2['name']?> 

													<ol>
													<!--3rd Level-->
													<?foreach ($aPages[$subpage2['id']] as $subpage3){?>
													<li data-id="<?=$subpage3['id']?>" data-name="<?=$subpage3['name']?>"><?=$subpage3['name']?>

														<!--4th Level-->
														<ol>
														<?foreach ($aPages[$subpage3['id']] as $subpage4){?>
														<li data-id="<?=$subpage4['id']?>" data-name="<?=$subpage4['name']?>"><?=$subpage4['name']?>

															<!--5th Level-->
															<ol>
															<?foreach ($aPages[$subpage4['id']] as $subpage5){?>
																<li data-id="<?=$subpage5['id']?>" data-name="<?=$subpage5['name']?>"><?=$subpage5['name']?></li>
															<?}?><!--End 5th Level-->
															</ol>

														</li>
														<?}?><!--End 4th Level-->
														</ol>

													</li>
													<?}?><!--End 3rd Level-->
													</ol>

												<?}?><!--End 2nd Level-->
												</li>
											</ol>
										<?}?><!--End 1st Level-->
										</li>
									</ol>

								</div>
							</div>
           
						</div>
						
						<!-- Modal Footer -->
						<div class="modal-footer">
						    <button type="button" class="btn btn-default" data-dismiss="modal"> Done</button>
						</div>

					</div><!--end modal content-->
				</div><!--end modal dialog-->
			</div><!--end modal-->

		<?= form_close();?>

	</div>


</div>
