<?php defined('BASEPATH') OR exit('No direct script access allowed');
 /*
 * Project:	  Activ CMS Version 5
 * File:	  admin/_header_view.php 
 * Author:    Marc Lorraine Jose
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 * Renders the global header
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=$this->config->item('language')?>">

	<head>

		<title>Activ Administration Control Panel</title>
	
		<meta http-equiv="Content-Type" content="text/html; <?=$this->config->item('charset')?>" />
		<meta http-equiv="content-language" content="<?=$this->config->item('language')?>" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<base href="<?=$this->config->item('site_url')?>admin/" />

		
		<!--TODO: Load all CSS on header -->

		<!--Favicon-->
		<link rel="shortcut icon" href="<?=base_url()?>assets/img/favicon.ico" type="image/x-icon">

		<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

		<!--Load Bootstrap 3.3.7 CDN-->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!--Load Fontawesome-->
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

		<!--Load Font-->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">


		<?if($isFancyBox){?>
		<!--Load FancyBox-->
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/plugins/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
		<?}?>

		<?if($isColorPicker){?>
		<!--Load ColorPicker-->
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/plugins/colorpicker/css/colorpicker.css"/>
		<?}?>

		<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/admin.css" title="Admin Style"/>
		<!-- Load Admin CSS -->
		<!--<?=assets_css('admin.css', array("title" => "Admin Style"));?> -->
		<script>var base_url = '<?php echo base_url() ?>';</script>

	</head> 
<body>

<!--Left Navigation Bar-->

<!--Container-->
<div id="controller">


		<div class="navigation_bar">
	
			<div class="visit_link">
				<a href="<?=base_url()?>" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i> <span>Visit Website</span></a>
			</div>

			<ul>

				<!--Dashboard-->
				<li <?if($aPageName == 'dashboard'){?>class="on"<?}?>>
					<a href="<?=base_url()?>admin/dashboard"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> <span>Activ Dashboard</span></a>
				</li><!--End Dashboard-->

				<!--Pages-->
				<li<?if($aPageName == 'pages' || $aPageName == 'sort_pages'){?>
						class="on"
					<?}?>>
					<a href="<?=base_url()?>admin/pages"><span><i class="fa fa-file-text" aria-hidden="true"></i> Pages</span></a>
				</li><!--End Pages-->

				<li><a><span><i class="fa fa-pencil" aria-hidden="true"></i> Components</span></a></li>

				<li><a><i class="fa fa-file-text-o" aria-hidden="true"></i> Blog Posts</span></a></li>

				<!--Plugins-->
				<li><a><span><i class="fa fa-wrench" aria-hidden="true"></i> Plugins</span></a></li>

				<!--<li><a><span><i class="fa fa-wrench" aria-hidden="true"></i> Tools</span></a></li>-->

				<!--Users-->
				<li <?if($aPageName == 'users'){?>
						class="on"
					<?}?>>
					<a href="<?=base_url()?>admin/users"><i class="fa fa-users" aria-hidden="true"></i> <span>Users</span></a>	

					<?if($aPageName == 'users'){?>
						<?=$aSubnav?>
					<?}?>
					
				</li><!--End Users-->

				<li <?if($aPageName == 'file_and_image'){?>class="on"<?}?>>
					<a href="<?=base_url()?>admin/file_and_image"><i class="fa fa-file-image-o" aria-hidden="true"></i> <span>Files & Images</span></a>
				</li>

				<!--Layout and Design-->
				<li <?if($aPageName == 'global_settings' || $aPageName == 'global_css' || $aPageName == 'global_js'){?>
						class="on"
					<?}?>>
					<a href="<?=base_url()?>admin/global_settings"><span><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Layout & Design</span></a>

					<?if($aPageName == 'global_settings' || $aPageName == 'global_css' || $aPageName == 'global_js'){?>
						<?=$aSubnav?>
					<?}?>
				</li><!--End Layout and Design-->

				<!--Site Settings-->
				<li <?if($aPageName == 'site_settings' || $aPageName == 'google_settings'){?>
						class="on"
					<?}?>>
					<a href="<?=base_url()?>admin/site_settings"><i class="fa fa-cogs" aria-hidden="true"></i> <span>Site Settings</span></a>

					<?if($aPageName == 'site_settings' || $aPageName == 'google_settings'){?>
						<?=$aSubnav?>
					<?}?>
					
				</li><!--EndSite Settings-->


			</ul>


			<div class="user_account">
				<a href="<?=base_url()?>admin/login/logout">Logout</a>
			</div>


			<div class="system_copyright">
				<p class="copyright">Copyright &copy; <?=date('Y')?> <a href="<?=$this->config->item('author_url')?>" target="_blank"><?=$this->config->item('author_name')?></a></p>
		        <p class="version">Version 5.0 Activ CMS</p>
			</div>

		</div>

		<div class="system_content">
		
			<!--Loader-->
			<div class="loader-div"><div class="loader"></div></div>
