<?php

 /*
 * Project:	  Activ CMS Version 5
 * File:	  dashboard_view.php
 * Author:    Activ Developers
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 */

?>
			<div class="col-md-10">
				<p class="system_intro">Welcome back <?=$this->session->userdata('admin')['firstname']?>!</p>
			</div>

			<div class="introduction col-md-3">
				<p class="intro_header">Control Panel Introduction</p>

				<p class="welcome">Welcome to the <strong>Activ Web Design</strong> control panel. </p>
				<div class="system_logo">
					<a href="http://www.activwebdesign.com/" target="_blank">
						<?=assets_img('activ_logo.png', array('alt' => 'Activ Web Design'))?>
					</a>
				</div>

				<div class="intro_text">
				<p>This is where you can create your website and make changes as and when you need them. 
				You will find detailed help on each page.</p>

				<p>if you require any technical assistance please contact us via our <a href="http://www.activwebdesign.com/support/" title="technical support" target="_blank">Support Site</a></p>
				</div>

			</div>

			<div class="introduction col-md-3 portlets">
				<p class="intro_header">Control Panel Introduction</p>

				<p class="welcome">Welcome to the <strong>Activ Web Design</strong> control panel. </p>
				<div class="system_logo">
					<a href="http://www.activwebdesign.com/" target="_blank">
						<?=assets_img('activ_logo.png', array('alt' => 'Activ Web Design'))?>
					</a>
				</div>

				<div class="intro_text">
				<p>This is where you can create your website and make changes as and when you need them. 
				You will find detailed help on each page.</p>

				<p>if you require any technical assistance please contact us via our <a href="http://www.activwebdesign.com/support/" title="technical support" target="_blank">Support Site</a></p>
				</div>

			</div>
