<?php
 /*
 * Project:	  Activ CMS Version 5
 * File:	  _header_view.php 
 * Author:    Activ Developers
 * Date		  December 2017
 * @copyright	2017 Activ Web Design 
 * Renders the global header
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=$this->config->item('site_language')?>">

<head>

	<title><?if(isset($aPage['settings']['meta_title'])){?><?=$aPage['settings']['meta_title']?><?}?></title><!--TODO-->
	<meta http-equiv="content-type" content="text/html; charset=<?=$this->config->item('charset')?>"/>
	<meta http-equiv="content-language" content="<?=$this->config->item('language')?>" />    

	<meta name="keywords" content="<?if(isset($aPage['settings']['meta_keys'])){?><?=$aPage['settings']['meta_keys']?><?}?>"/><!--TODO-->
	<meta name="description" content="<?if(isset($aPage['settings']['meta_desc'])){?><?=$aPage['settings']['meta_desc']?><?}?>"/><!--TODO-->

	<meta name="author" content="<?=$this->config->item('author_name')?> - <?=$this->config->item('author_url')?>"/> 

	<!--Search Engine Index-->
	<?if($this->config->item('sei')){?>
	<meta name="robots" content="index, follow"/>
	<meta name="revisit-after" content="2 Days"/>		
	<?}else{?>
	<meta name="robots" content="noindex, nofollow"/>
	<?}?>

	<?if($this->config->item('google_verifier')){?>
	<!--Google Verifier-->
	<?=$this->config->item('google_verifier')?>
	<?}?>

	<?if($this->config->item('is_responsive') == 1){?>
	<!--Responsive Viewport-->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?}?>

	<!--Favicon-->
	<?if($this->config->item('site_favicon')){?>
	<link rel="shortcut icon" href="<?=$this->config->item('site_favicon')?>" type="image/x-icon">  
	<?}else{?>
	<link rel="shortcut icon" href="<?=base_url()?>assets/images/favicon.ico" type="image/x-icon">  
	<?}?>
	<base href="<?=base_url()?>" />

	     
	<?if($this->config->item('news_active')){?><!--TODO-->
	<link rel="alternate" type="application/rss+xml" href="<?=$this->config->item('base_url')?><?=$this->config->item('news_page_path')?>/rss"/>
	<?}?>

	<!--TODO-->
	<?if($aPage['id'] > 1){
	echo "\n";?>
	<!-- Prevent duplicate pages being indexed -->
	<link rel="canonical" href="<?=base_url().ltrim($aPage['path'], '/')?><?=$this->config->item('file_ext')?>"/>
	<?}?>
	<!--TODO Bootstrap-->

</head>


<body>

	<div class="container">
