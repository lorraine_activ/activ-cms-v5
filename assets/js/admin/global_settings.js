$(document).ready(function(){
	
	$(".open_filemanager").fancybox({	
		'width'		: 900,
		'height'	: 600,
		'type'		: 'iframe',
        'autoScale' : false
    });

	$('.width_selection').click(function(){
		if($(this).val() == 'px'){
			$('.wesite_percent_width_div').slideUp();
			$('.wesite_pixel_width_div').slideDown();			
			
		}else{
			$('.wesite_percent_width_div').slideDown();
			$('.wesite_pixel_width_div').slideUp();
		}
	})
	
  	/* set bg colors and text colors of color fields */
  
  	$('#default_font_colour').css('backgroundColor', $('#default_font_colour').val());
	if(h2d($('#default_font_colour').val().substr(1)) < 8388607.5) {
		$('#default_font_colour').css('color', '#ffffff');
	}else{
		$('#default_font_colour').css('color', '#000000');
	}
  
  	$('#default_link_colour').css('backgroundColor', $('#default_link_colour').val());
	if(h2d($('#default_link_colour').val().substr(1)) < 8388607.5) {
		$('#default_link_colour').css('color', '#ffffff');
	}else{
		$('#default_link_colour').css('color', '#000000');
	}
	
	/*-- Default Font Colour --*/
  	$('#default_font_colour').ColorPicker({
		onSubmit: function(hsb, hex, rgb) {
			$('#default_font_colour').val('#'+hex);
			$('#default_font_colour').css('backgroundColor', '#'+hex)
			if (h2d(hex) < 8388607.5) {
				$('#default_font_colour').css('color', '#ffffff');
			} else {
				$('#default_font_colour').css('color', '#000000');
			}
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		}
	})
	.bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	});

	
	/*-- Default Link Colour --*/
	$('#default_link_colour').ColorPicker({
		onSubmit: function(hsb, hex, rgb) {
			$('#default_link_colour').val('#'+hex);
			$('#default_link_colour').css('backgroundColor', '#'+hex)
			if (h2d(hex) < 8388607.5) {
				$('#default_link_colour').css('color', '#ffffff');
			} else {
				$('#default_link_colour').css('color', '#000000');
			}
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		}
	})
	.bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	});
	

	/*-- Font Size --*/
  	var default_font_size = $("#current_default_font_size").val(); 
  	$("#default_font_size_slider").slider({
			value:12,
			min: 10,
			max: 20,
			step: 1,
			slide: function(event, ui) {
				$("#default_font_size").val(ui.value+'px');
			}
	});

	$("#default_font_size").val($("#default_font_size_slider").slider("value")+'px');
  	$("#default_font_size_slider").slider('option', 'value', default_font_size);
  	$("#default_font_size").val(default_font_size+"px");


	/*-- Webiste Pixel Width --*/
  	var current_px_value = $("#current_px_value").val(); 
  	$("#wesite_pixel_width_slider").slider({
			value:780,
			min: 500,
			max: 1200,
			step: 10,
			slide: function(event, ui) {
				$("#website_px_width").val(ui.value+'px');
			}
	});
	$("#website_px_width").val($("#wesite_pixel_width_slider").slider("value")+'px');
  	$("#wesite_pixel_width_slider").slider('option', 'value', current_px_value);
  	$("#website_px_width").val(current_px_value+"px");



	/*-- Webiste Percent Width --*/
  	var current_percent_value = $("#current_percent_value").val(); 
  	$("#wesite_percent_width_slider").slider({
			value:100,
			min: 50,
			max: 100,
			step: 5,
			slide: function(event, ui) {
				$("#website_percent_width").val(ui.value+'%');
			}
	});
	$("#website_percent_width").val($("#wesite_percent_width_slider").slider("value")+'%');
  	$("#wesite_percent_width_slider").slider('option', 'value', current_percent_value);
  	$("#website_percent_width").val(current_percent_value+"%");


	/*-- Functions --*/
	function h2d(h) {
		return parseInt(h,16);
  	} 

});
