$(document).ready(function(){
	
	/*-- Sort Pages --*/
	var group = $("ol.sort_pages").sortable({
	  group: 'sort_pages',
	  delay: 500,
	  onDrop: function ($item, container, _super) {
	  	
		//show loading icon
		$('.loader-div').hide().fadeIn(500);
	  	
		var data = group.sortable("serialize").get();

		var jsonString = JSON.stringify(data, null, ' ');

		var arrayString = $.parseJSON(jsonString);

		//$('#serialize_output2').text(jsonString);
		_super($item, container);
		//console.log(jsonString);

		//call ajax

			//fetch data
			var sortPages = fetchData(
			{
				'action': 'sort-pages',
				'seq': jsonString
				
			});
		
			// If ajax call is succesful
			$.when(sortPages).then(function(data) {
				// If successful
				$('.loader-div').hide().fadeOut(800);
				//$('.sort_pages').prepend(formSuccess('The page order was updated successfully.'));
				//$('#formSuccess').delay('1000').fadeOut(1000);

			});

	  }//onDrop
	});

	$('#sortpages_modal').on('hide.bs.modal', function (e) {
		window.location.reload();
	})


	$('.type_selection').click(function(){
		
		if($(this).val() == '1'){
			$('.custom_page_div').slideUp();		
			$('.auto_page_div').slideDown();	
			
		}else{
			$('.custom_page_div').slideDown();
			$('.auto_page_div').slideUp();
		}
	})

	/*-- if page is selected --*/
	$('#page_id').change(function(e){
		if($(this).val() == ''){
			resetFields();
		}

		$('#form_pages').submit();	
	})


	/*-- Delete page --*/
	$('button#page_delete').click(function(e){
		e.preventDefault();
		//pop dialog
		deleteDialog();
	})//end



	//function
	function deleteDialog(){
		
		$( ".delete_dialog" ).dialog({
			resizable: false,
		  	height: "auto",
		  	width: 400,
		  	modal: true,
		  	buttons: {
				"Delete Page": function() {
					resetFields();
					$('#action').val('delete');	
				  	$('#form_pages').submit();
				},
				Cancel: function() {
				  	$( this ).dialog( "close" );
				}
		  	}
		});	
	}//end

	//function
	function resetFields(){
		//reset
		$("#parent_id").val('').change();	
		$('#name').val('');
		$('#is_private').attr('checked', false);
		$('#is_hidden_on_nav').attr('checked', false);
		$('#hide_main_nav').attr('checked', false);
		$('#title').val('');
		$('#metadesc').val('');
		$('#metakeys').val('');
		$('#redirect_script').val('');
	}//end


	// Generic function to make an AJAX call
	var fetchData = function(query) {
		// Return the $.ajax promise
		return $.ajax({
			type: "POST",
			url: base_url + 'admin/fetch',
		    data: query,
		});
	}

	function formSuccess(msg){
		var successMsg = '<div id="formSuccess">'+msg+'</div>';
		return successMsg;
	}//end

});
