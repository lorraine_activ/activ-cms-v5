$(document).ready(function(){
	
		editAreaLoader.init({
			id: "advance_css"	// id of the textarea to transform	
			,start_highlight: true	
			,font_size: "8"
			,font_family: "verdana, monospace"
			,allow_resize: "both"
			,allow_toggle: true
			,language: "en"
			,syntax: "css"	
			,toolbar: "search, go_to_line, |, undo, redo, |, select_font, |, change_smooth_selection, highlight, reset_highlight, |, help"
			,min_height: 350
				
		});

});
