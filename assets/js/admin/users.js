$(document).ready(function(){

	//if users is selected
	$('#user_id').change(function(e){
		if($(this).val() == ''){
			resetFields();
		}

		$('#form_users').submit();	
	})

	//if roles was changed
	$('select#user_role').change(function(e){
		if($(this).val() < 4){
			$('.is_approved_div').hide();
		}else{
			$('.is_approved_div').show();
		}
	})


	//Delete users
	$('button#users_delete').click(function(e){
		e.preventDefault();
		//pop dialog
		deleteDialog();
	})//end


	//export buttons
	$('#export_buttons').click(function(e){
		e.preventDefault();
		if(!$('#export_role').val()){
			alert('Please select user role!');
		}else{
			$('#form_users_download').submit();
			$('#exportFormModal').modal('hide');			
		}		
	})

	//function
	function deleteDialog(){
		
		$( ".delete_dialog" ).dialog({
			resizable: false,
		  	height: "auto",
		  	width: 400,
		  	modal: true,
		  	buttons: {
				"Delete User": function() {
					resetFields();
					$('#action').val('delete');	
				  	$('#form_users').submit();
				},
				Cancel: function() {
				  	$( this ).dialog( "close" );
				}
		  	}
		});	
	}//end

	//function
	function resetFields(){
		//reset
		$("#user_role").val('').change();	
		$('#firstname').val('');
		$('#lastname').val('');
		$('#email').val('');
		$('#user_password').val('');
		$('#user_confirm').val('');
		$('#is_approved').val('');

	}//end

});
